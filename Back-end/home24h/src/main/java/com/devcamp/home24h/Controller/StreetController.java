package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.Street;
import com.devcamp.home24h.Repository.StreetRepository;
import com.devcamp.home24h.Service.StreetService;

@RestController
@CrossOrigin
public class StreetController {
    @Autowired
    StreetService streetService;
    
    @Autowired
    StreetRepository iStreet;

    @GetMapping("/streets")
    public ResponseEntity<Object> filterWard(@RequestParam(defaultValue = "0") int provinceId,
                                               @RequestParam(defaultValue = "0") int districtId,
                                               @RequestParam(defaultValue = "") String name,
                                               @RequestParam(defaultValue = "0") int page,
                                               @RequestParam(defaultValue = "10") int size) {
        try {
            Page<Street> streets = streetService.filterAndPaginationForStreet(provinceId, districtId, name, page, size);
            return new ResponseEntity<>(streets, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get street by id
    @GetMapping("/streets/{id}")
    public ResponseEntity<Street> getStreetsById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iStreet.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get all street by district id
    @GetMapping("/districts/{id}/streets")
    public ResponseEntity<List<Street>> getAllStreetsByDistrictId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iStreet.getAllWardByDistrictId(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/districts/{districtId}/streets")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Street> createStreet(@Valid @RequestBody Street street, @PathVariable("districtId") int districtId) {
        try {
            Street createdStreet = streetService.createStreet(street, districtId);
            return new ResponseEntity<>(createdStreet, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/districts/{districtId}/streets/{streetId}")
    public ResponseEntity<Street> updateStreet(@Valid @RequestBody Street street, @PathVariable("districtId") int districtId, @PathVariable("streetId") int streetId) {
        try {
            Street updatedStreet = streetService.updateStreet(street, streetId, districtId);
            return new ResponseEntity<>(updatedStreet, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/streets/{id}")
    public ResponseEntity<Street> deleteStreet(@PathVariable("id") int id) {
        try {
            streetService.deleteStreet(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

