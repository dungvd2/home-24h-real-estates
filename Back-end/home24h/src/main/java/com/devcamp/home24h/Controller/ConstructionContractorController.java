package com.devcamp.home24h.Controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.ConstructionContractor;
import com.devcamp.home24h.Repository.ConstructionContractorRepository;
import com.devcamp.home24h.Service.ConstructionContractorService;
import com.devcamp.home24h.Service.ValidateService;

@RestController
@CrossOrigin
public class ConstructionContractorController {
    @Autowired
    ConstructionContractorService ccService;

    @Autowired
    ConstructionContractorRepository iConstructionContractor;

    @Autowired 
    ValidateService validateService;

    // API lấy ra danh sách các nhà thầu xây dựng có lọc, phân trang
    @GetMapping("/construction-contractors")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAllByPageAndSize(@RequestParam(defaultValue = "0") int page, 
                                                      @RequestParam(defaultValue = "10") int size,
                                                      @RequestParam(defaultValue = "") String keyword,
                                                      @RequestParam(defaultValue = "id") String sortBy,
                                                      @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<ConstructionContractor> constructionContractors = ccService.getAllConstructionContractorsByPage(keyword, sortBy, sortType, page, size);
            return new ResponseEntity<>(constructionContractors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra danh sách các nhà thầu theo list
    @GetMapping("/construction-contractors/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAll() {
        try {
            List<ConstructionContractor> constructionContractors = iConstructionContractor.findAll();
            return new ResponseEntity<>(constructionContractors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra thông tin 1 nhà thầu thông qua ID
    @GetMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ConstructionContractor> getConstructionContractorById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iConstructionContractor.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    // API tạo mới 1 nhà thầu
    @PostMapping("/construction-contractors")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createConstructionContractor(@Valid @RequestBody ConstructionContractor constructionContractor) {
        try {
            String vMessageValidate = validateService.validateEmailAndPhoneNumber(constructionContractor.getEmail(), constructionContractor.getPhone(), constructionContractor.getPhone2());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(vMessageValidate);
            }
            ConstructionContractor createdConstructionContractor = ccService.createConstructionContractor(constructionContractor);
            return new ResponseEntity<>(createdConstructionContractor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>("Có lỗi xảy ra, vui lòng thử lại sau", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API cập nhật 1 nhà thầu
    @PutMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateConstructionContractor(@Valid @RequestBody ConstructionContractor constructionContractor, @PathVariable("id") int id) {
        try {
            String vMessageValidate = validateService.validateEmailAndPhoneNumber(constructionContractor.getEmail(), constructionContractor.getPhone(), constructionContractor.getPhone2());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(vMessageValidate);
            }
            ConstructionContractor updatedConstructionContractor = ccService.updateConstructionContractor(constructionContractor, id);
            return new ResponseEntity<>(updatedConstructionContractor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>("Có lỗi xảy ra, vui lòng thử lại sau", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa 1 nhà thầu
    @DeleteMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ConstructionContractor> deleteConstructionContractor(@PathVariable("id") int id) {
        try {
            ccService.deleteConstructionContractor(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

