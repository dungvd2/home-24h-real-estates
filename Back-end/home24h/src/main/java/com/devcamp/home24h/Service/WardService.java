package com.devcamp.home24h.Service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.District;
import com.devcamp.home24h.Entity.Ward;
import com.devcamp.home24h.Repository.DistrictRepository;
import com.devcamp.home24h.Repository.WardRepository;

@Service
public class WardService {
    @Autowired
    WardRepository iWard;

    @Autowired
    DistrictRepository iDistrict;

    // Hàm lấy ra toàn bộ ward
    public Page<Ward> filterAndPaginationForWard(int provinceId, int districtId, String name, int page, int size)  {    
        Pageable pageable = PageRequest.of(page, size);
        if(provinceId == 0) {
            return iWard.findByNameContainingIgnoreCase(name, pageable);
        }
        else if(districtId == 0) {
            return iWard.filterWardByProvinceIdAndName(name, provinceId, pageable);
        }
        else {
            return iWard.filterWardByDistrictIdAndName(name, districtId, pageable);
        }
    }

    // Hàm tạo mới 1 ward
    public Ward createWard(Ward pWard, Integer districtId) {
        District vDistrict = iDistrict.findById(districtId).get();
        pWard.setDistrict(vDistrict);
        pWard.setProvinceId(vDistrict.getProvinceId());
        Ward wardCreated = iWard.save(pWard);
        return wardCreated;
    }

    // Hàm cập nhật 1 ward
    public Ward updateWard(Ward pWard, Integer wardId, Integer districtId) {
        Ward vWard = iWard.findById(wardId).get();
        District vDistrict = iDistrict.findById(districtId).get();
        vWard.setDistrict(vDistrict);
        vWard.setName(pWard.getName());
        vWard.setPrefix(pWard.getPrefix());
        vWard.setProvinceId(vDistrict.getProvinceId());
        Ward updatedWard = iWard.save(vWard);
        return updatedWard;
    }

    // Hàm xóa 1 ward
    public void deleteWard(Integer wardId){
        iWard.deleteById(wardId);
    }
}
