package com.devcamp.home24h.Service;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.BaseEntity;
import com.devcamp.home24h.Entity.Role;
import com.devcamp.home24h.Entity.User;
import com.devcamp.home24h.Repository.RoleRepository;
import com.devcamp.home24h.Repository.UserRepository;
import com.devcamp.home24h.security.UserPrincipal;

@Service
public class AccountService {
    @Autowired
    private UserRepository iUser;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordResetService passwordResetService;

    @Autowired
    private RoleRepository iRole;

    // Hàm kiểm tra xem username, email, sđt đăng ký có trùng hay không
    // Input: đối tượng user bất kì
    // Lần lượt tìm ra user bằng username, email, sđt nếu đã tồn tại trả về lỗi 400
    // kèm cảnh báo nếu trùng
    // Nếu không trả về null
    public ResponseEntity<Object> checkForDuplicates(User user) {
        User userCheck = iUser.findByUsername(user.getUsername());
        if (userCheck != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tên đăng nhập đã được sử dụng!");
        }

        User userCheckEmail = iUser.findByEmail(user.getEmail());
        if (userCheckEmail != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email đã được đăng ký bởi tài khoản khác!");
        }

        User userCheckPhone = iUser.findByPhoneNumber(user.getPhoneNumber());
        if (userCheckPhone != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Số điện thoại đã được đăng ký bởi tài khoản khác!");
        }
        return null; // Không có trùng lặp
    }

    // Hàm kiểm tra xem email, sđt có trùng hay không
    // Input: đối tượng user bất kì
    // Lần lượt tìm ra user bằng email, sđt nếu đã tồn tại trả về lỗi 400
    // kèm cảnh báo nếu trùng
    // Nếu không trả về null
    public ResponseEntity<Object> checkForDuplicatesEmailOrPhone(User user) {
        User userCheckEmail = iUser.findByEmail(user.getEmail());
        if (userCheckEmail != null && !userCheckEmail.getUsername().equals(user.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email đã được đăng ký bởi tài khoản khác!");
        }

        User userCheckPhone = iUser.findByPhoneNumber(user.getPhoneNumber());
        if (userCheckPhone != null && !userCheckPhone.getUsername().equals(user.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Số điện thoại đã được đăng ký bởi tài khoản khác!");
        }
        return null; // Không có trùng lặp
    }

    // Hàm dành cho admin tạo tài khoản
    // Input: Đối tượng user, roleKey, passCode tạo ra gửi về email
    // Output: Nếu có role customer thì ko cần phải duyệt, còn nếu không thì phải
    // xét duyệt passcode nếu pass thì mới tạo tài khoản
    public ResponseEntity<Object> checkAndCreateUser(User user, String roleKey, String passCode) {
        // Tìm role theo rolekey
        Role role = iRole.findByRoleKey(roleKey);
        user.getRoles().add(role);
        // Mã hóa thiết lập mật khẩu bằng hàm BcryptPasswordEncoder
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        if (roleKey.equals("ROLE_CUSTOMER")) {
            userService.createUser(user);
            return ResponseEntity.ok("Tạo mới tài khoản thành công!");
        }
        // Thông qua Token lấy ra useradmin
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        User userAdmin = iUser.findByUsername(userPrincipal.getUsername());
        // Check mã bảo vệ
        Boolean isPassCodeValid = passwordResetService.isPassCodeValid(userAdmin, passCode);
        if (!isPassCodeValid) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ không hợp lệ!");
        }
        userService.createUser(user);
        return null;
    }


    // Hàm dành cho admin cập nhật tài khoản
    // Input: Đối tượng user, roleKey, passCode tạo ra gửi về email
    // Output: Nếu có role customer thì ko cần phải duyệt, còn nếu không thì phải
    // xét duyệt passcode nếu pass thì mới cho sửa và lưu
    public ResponseEntity<Object> checkAndUpdateUser(String username, User pUser, String roleKey, String passCode) {
        // Lấy ra user bằng username
        User user = iUser.findByUsername(username);
        List<Role> currentRoles = new ArrayList<>(user.getRoles());
        // Lấy ra role hiện tại
        String currentRoleKey = currentRoles.get(0).getRoleKey();

        // Tìm role theo rolekey
        Role role = iRole.findByRoleKey(roleKey);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        user.setRoles(roles);
        user.setFirstName(pUser.getFirstName());
        user.setLastName(pUser.getLastName());
        // Mã hóa thiết lập mật khẩu bằng hàm BcryptPasswordEncoder
        user.setPassword(new BCryptPasswordEncoder().encode(pUser.getPassword()));
        user.setEmail(pUser.getEmail());
        user.setPhoneNumber(pUser.getPhoneNumber());
        
        // Nếu không thay đổi role và role hiện tại là customer thì khỏi phải xét passcode
        if (roleKey.equals("ROLE_CUSTOMER") && roleKey.equals(currentRoleKey)) {
            iUser.save(user);
            return null;
        }

        // Thông qua Token lấy ra useradmin
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        User userAdmin = iUser.findByUsername(userPrincipal.getUsername());
        // Check mã bảo vệ
        Boolean isPassCodeValid = passwordResetService.isPassCodeValid(userAdmin, passCode);
        if (!isPassCodeValid) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ không hợp lệ!");
        }
        iUser.save(user);
        return null;
    }

    // Hàm thay đổi role của user
    public ResponseEntity<Object> changeRoleUser(String username, String roleKey, String passCode) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
        User user = iUser.findByUsername(userPrincipal.getUsername());
        Boolean isPassCodeValid = passwordResetService.isPassCodeValid(user, passCode);
        if (!isPassCodeValid) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Mã bảo vệ không chính xác vui lòng thử lại sau!");
        }
        Role role = iRole.findByRoleKey(roleKey);
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        User userChange = iUser.findByUsername(username);
        userChange.setRoles(roles);
        // Lấy ra thời gian hiện tại, set cho thời gian hết hạn, tức là đã hết hạn
        LocalDateTime currentTimestamp = LocalDateTime.now();
        user.setPassCodeExpiredTime(currentTimestamp);
        iUser.save(user);
        return null;
    }


    // Hàm lấy thông tin tên của cột trên db thông qua attribute của USER
    public String getColumnName(String fieldName) {
        try {
            Field field = User.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Hàm lấy thông tin tên của cột trên db thông qua attribute của baseEntity
    public String getColumnNameBaseEntity(String fieldName) {
        try {
            Field field = BaseEntity.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Hàm lấy ra danh sách user theo các tiêu chí có phân trang
    // Có 2 class 1 là baseEntity và user xem sortType thuộc ên nào thông qua hàm contain của list
    public Page<User> filterUser(String username, String name, String roleKey, int deleted, String keyword, String sortBy, String sortType, int page, int size) {

        List<String> listBaseEntityColumn = Arrays.asList("createdAt", "updatedAt", "updatedBy", "deleted");
        String filed = null;
        if(listBaseEntityColumn.contains(sortBy)) {
            filed = getColumnNameBaseEntity(sortBy);
        }
        else {
            filed = this.getColumnName(sortBy);
        }
        // Nếu không tồn tại thì mặc định là sắp xếp theo id
        if(filed == null) {
            filed = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filed);
        Pageable pageable = PageRequest.of(page, size, sort);
        Page<User> users = iUser.filterUserByUsernameNameRoleAndDeleted(username, name, deleted, roleKey, keyword, pageable);
        return users;
    }


    // Hàm kiểm tra xem user đủ điều kiện đăng ký hay không kiểm tra username, email sđt xem có trùng vs ai không, nếu không thì cho đăng ký
    // Nếu có trả về thông báo lỗi
    public ResponseEntity<Object> checkAndRegisterUser(User user) {
        String username = user.getUsername();
        String email = user.getEmail();
        String phone = user.getPhoneNumber();

        User userCheck = iUser.findByUsername(username);
        if(userCheck != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tên đăng nhập đã được sử dụng!");
        }

        User userCheckByEmail = iUser.findByEmail(email);
        if(userCheckByEmail != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email đã được đăng ký bởi tài khoản khác!");
        }

        User userCheckByPhone = iUser.findByPhoneNumber(phone);
        if(userCheckByPhone != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Số điện thoại đã được đăng ký bởi tài khoản khác!");
        }
        
        Role roleCustomer = iRole.findByRoleKey("ROLE_CUSTOMER");
        user.getRoles().add(roleCustomer);
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userService.createUser(user);
        return ResponseEntity.ok("Tạo tài khoản thành công!");
    }

    // Hàm kiểm tra email và số điện thoại xem có trùng hay không, nếu không thì cho update, nếu có thì trả về lỗi
    public ResponseEntity<Object> checkAndUpdateUserByUser(User user, User pUser) {
        String username = user.getUsername();
        String email = pUser.getEmail();
        String phone = pUser.getPhoneNumber();


        User userCheckByEmail = iUser.findByEmail(email);
        if(userCheckByEmail != null && !userCheckByEmail.getUsername().equals(username)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email đã được đăng ký bởi tài khoản khác!");
        }

        User userCheckByPhone = iUser.findByPhoneNumber(phone);
        if(userCheckByPhone != null && !userCheckByPhone.getUsername().equals(username)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Số điện thoại đã được đăng ký bởi tài khoản khác!");
        }

        user.setEmail(email);
        user.setPhoneNumber(phone);
        user.setFirstName(pUser.getFirstName());
        user.setLastName(pUser.getLastName());

        iUser.save(user);
        return ResponseEntity.ok("Cập nhập thông tin tài khoản thành công!");
    }

    // Hàm validate mật khẩu
    public Boolean validatePassword(String paramPassword) {
        // Define the regular expression pattern
        String passwordPattern = "^(?=.*[a-zA-Z])(?=.*[0-9]).{6,}$";
        
        // Compile the regular expression
        Pattern pattern = Pattern.compile(passwordPattern);
        
        // Create a Matcher object for the input string
        Matcher matcher = pattern.matcher(paramPassword);
        
        // Use the Matcher's find method to check for a match
        return matcher.find();
    }
}
