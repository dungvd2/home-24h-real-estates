package com.devcamp.home24h.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

@Service
public class ValidateService {

    // Hàm validate số điện thoại nhập vào
    public Boolean validatePhoneNumber(String phoneNumber) {
        String regexPattern = "^0\\d{9}$";
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    // Hàm validate email nhập vào
    public Boolean validateEmail(String email) {
        String regexPattern = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regexPattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // Hàm validate email và số điện thoại
    public String validateEmailAndPhoneNumber(String email, String phoneNumber, String phoneNumber2) {
        String result = "ok";
        if(!validatePhoneNumber(phoneNumber)) {
            return "Số điện thoại không đúng";
        } 
        
        if(!validatePhoneNumber(phoneNumber2) && !phoneNumber2.equals("")) {
            return "Số điện thoại 2 không đúng";
        }

        if(!email.equals("") && !validateEmail(email)) {
            return "Email nhập vào không hợp lệ";
        }

        return result;
    }

    // Hàm validate email và số điện thoại cho user
    public String validateEmailAndPhoneNumberForUser(String email, String phoneNumber) {
        String result = "ok";

        if(!validatePhoneNumber(phoneNumber)) {
            return "Số điện thoại không đúng";
        } 

        if(!validateEmail(email)) {
            return "Email nhập vào không hợp lệ";
        }
        return result;
    }


}
