package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.DesignUnit;
import com.devcamp.home24h.Repository.DesignUnitRepository;
import com.devcamp.home24h.Service.DesignUnitService;
import com.devcamp.home24h.Service.ValidateService;

@RestController
@CrossOrigin
public class DesignUnitController {
    @Autowired
    DesignUnitService designUnitService;

    @Autowired
    DesignUnitRepository iDesignUnit;

    @Autowired
    ValidateService validateService;

    // API lấy ra toàn bộ đơn vị thiết kế có phân trang
    @GetMapping("/designunits")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAllDesignUnitByPageAndSize(@RequestParam(defaultValue = "0") int page, 
                                                                @RequestParam(defaultValue = "10") int size,
                                                                @RequestParam(defaultValue = "") String keyword,
                                                                @RequestParam(defaultValue = "id") String sortBy,
                                                                @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<DesignUnit> designUnits = designUnitService.getAllDesignUnitByPage(keyword, sortBy, sortType, page, size);
            return new ResponseEntity<>(designUnits, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra toàn bộ đơn vị thiết kế không phân trang
    @GetMapping("/designunits/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAll() {
        try {
            List<DesignUnit> designUnits = iDesignUnit.findAll();
            return new ResponseEntity<>(designUnits, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get design unit by id
    @GetMapping("/designunits/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<DesignUnit> getDesginUnitById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iDesignUnit.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 design unit
    @PostMapping("/designunits")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody DesignUnit designUnit) {
        try {
            String vMessageValidate = validateService.validateEmailAndPhoneNumber(designUnit.getEmail(), designUnit.getPhone(), designUnit.getPhone2());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(vMessageValidate);
            }
            DesignUnit createdDesignUnit = designUnitService.createDesignUnit(designUnit);
            return new ResponseEntity<>(createdDesignUnit, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API sửa 1 đơn vị thiết kế
    @PutMapping("/designunits/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateDesignUnit(@Valid @RequestBody DesignUnit designUnit, @PathVariable("id") int id) {
        try {
            String vMessageValidate = validateService.validateEmailAndPhoneNumber(designUnit.getEmail(), designUnit.getPhone(), designUnit.getPhone2());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(vMessageValidate);
            }
            DesignUnit updatedDesignUnit = designUnitService.updateDesignUnit(designUnit, id);
            return new ResponseEntity<>(updatedDesignUnit, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa 1 đơn vị thiết kế
    @DeleteMapping("/designunits/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<DesignUnit> deleteDesignUnit(@PathVariable("id") int id) {
        try {
            designUnitService.deleteDesignUnit(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

