package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.Investor;
import com.devcamp.home24h.Repository.InvestorRepository;
import com.devcamp.home24h.Repository.ProjectRepository;
import com.devcamp.home24h.Service.InvestorService;
import com.devcamp.home24h.Service.ValidateService;

@RestController
@CrossOrigin
public class InvestorController {
    @Autowired
    InvestorService investorService;


    @Autowired
    InvestorRepository iInvestor;

    @Autowired
    ProjectRepository iProject;

    @Autowired
    ValidateService validateService;

    @GetMapping("/investors")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAllInvestorByPageAndSize(@RequestParam(defaultValue = "0") int page, 
                                                              @RequestParam(defaultValue = "10") int size,
                                                              @RequestParam(defaultValue = "") String keyword,
                                                              @RequestParam(defaultValue = "id") String sortBy,
                                                              @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<Investor> investors = investorService.getAllByPageAndSize(keyword, sortBy, sortType, page, size);
            return new ResponseEntity<>(investors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAll() {
        try {
            List<Investor> investors = iInvestor.findAll();
            return new ResponseEntity<>(investors, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Investor> getInvestorById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iInvestor.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/investors")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createInvestor(@Valid @RequestBody Investor investor) {
        try {
            String vMessageValidate = validateService.validateEmailAndPhoneNumber(investor.getEmail(), investor.getPhone(), investor.getPhone2());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(investor);
            }
            Investor createdInvestor = investorService.createInvestor(investor);
            return new ResponseEntity<>(createdInvestor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/investors/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateInvestor(@Valid @RequestBody Investor investor, @PathVariable("id") int id) {
        try {
            String vMessageValidate = validateService.validateEmailAndPhoneNumber(investor.getEmail(), investor.getPhone(), investor.getPhone2());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(investor);
            }
            Investor updatedInvestor = investorService.updateInvestor(investor, id);
            return new ResponseEntity<>(updatedInvestor, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    // Xóa nhà đầu tư, đếm số dự án đang được đầu tư, nếu không có thì cho xóa, nếu có thì thông báo và không cho xóa
    @DeleteMapping("/investors/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteInvestor(@PathVariable("id") int id) {
        try {
            Long countProject = iProject.countByInvestor(id);
            if(countProject == 0) {
                investorService.deleteInvestor(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return ResponseEntity.badRequest().body("Có " + countProject + " dự án đang được đầu tư bởi nhà đầu tư này, không thể xóa!");
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>("Có lỗi xảy ra, vui lòng thử lại sau", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

