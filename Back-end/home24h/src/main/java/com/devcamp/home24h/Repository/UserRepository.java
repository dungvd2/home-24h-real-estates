package com.devcamp.home24h.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.User;


public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByPhoneNumber(String phoneNumber);
    
    User findByEmail(String email);
    
    // Hàm lọc tất cả khách hàng theo username, name, rolekeys, deleted
    @Query(value = "SELECT u.* FROM `t_user` u " +
                    "JOIN  `t_user_role` ur ON u.id = ur.user_id " +
                    "JOIN `t_role` r ON ur.role_id = r.id " + 
                    "WHERE u.username LIKE %:username% " +
                    "AND (:name = '' OR CONCAT(u.first_name, ' ', u.last_name) LIKE %:name%) " +
                    "AND (:deleted = 2 OR u.deleted = :deleted) " +
                    "AND r.role_key LIKE %:roleKey% " +
                    "AND (u.username LIKE %:keyword% " + 
                    "OR u.first_name LIKE %:keyword% "+
                    "OR u.last_name LIKE %:keyword% "+
                    "OR u.email_address LIKE %:keyword% "+
                    "OR u.phone_number LIKE %:keyword%)", nativeQuery = true)
    Page<User> filterUserByUsernameNameRoleAndDeleted(@Param("username") String username,
                                                      @Param("name") String name,
                                                      @Param("deleted") int deleted,
                                                      @Param("roleKey") String roleKey,
                                                      @Param("keyword") String keyword, Pageable pageable);
}
