package com.devcamp.home24h.Service;

import java.lang.reflect.Field;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.Utilities;
import com.devcamp.home24h.Repository.UtilitiesRepository;

@Service
public class UtilitesService {
    @Autowired
    UtilitiesRepository iUtilities;

    @Autowired
    ProjectService projectService;

    // lấy ra toàn bộ uti
    public Page<Utilities> getAllByPage(int page, int size, String keyword, String sortBy, String sortType) {
         String filled = getColumnName(sortBy);
        if(filled == null) {
            filled = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filled);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iUtilities.findAllByKeyword(keyword, pageable);
    }

    // Tạo mới 1 utiti
    public Utilities createUtilities(Utilities pUtilities) {
        Utilities createdUtilities = iUtilities.save(pUtilities);
        return createdUtilities;
    }

    // Cập nhật 1 utili
    public Utilities updateUtilites(Utilities pUtilities, int id) {
        Utilities vUtilities = iUtilities.findById(id).get();
        vUtilities.setDescription(pUtilities.getDescription());
        vUtilities.setName(pUtilities.getName());
        vUtilities.setPhoto(pUtilities.getPhoto());
        Utilities updatedUtilities = iUtilities.save(vUtilities);
        return updatedUtilities;
    }

    // xóa 1 utili
    public void deleteUtilities(int id) {
        projectService.updateProjectWhenDeleteUtilites(id);
        iUtilities.deleteById(id);
    }

    public String getColumnName(String fieldName) {
        try {
            Field field = Utilities.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
