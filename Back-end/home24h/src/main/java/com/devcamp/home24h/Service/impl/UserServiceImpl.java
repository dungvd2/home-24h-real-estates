package com.devcamp.home24h.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.User;
import com.devcamp.home24h.Repository.UserRepository;
import com.devcamp.home24h.security.UserPrincipal;
import com.devcamp.home24h.Service.UserService;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public UserPrincipal findByUsername(String username) {
        User user = userRepository.findByUsername(username);
        UserPrincipal userPrincipal = new UserPrincipal();
        if (null != user) {
            Set<String> authorities = new HashSet<>();
            if (null != user.getRoles()) user.getRoles().forEach(r -> {
                authorities.add(r.getRoleKey());
            });

            userPrincipal.setUserId(user.getId());
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
            userPrincipal.setAuthorities(authorities);
            String fullName = "";
            if(user.getFirstName() != null) {
                fullName += user.getFirstName();
            }
            if(user.getLastName() != null) {
                fullName += " " + user.getLastName();
            }
            userPrincipal.setFullName(fullName);
        }
        return userPrincipal;
    }

}
