package com.devcamp.home24h.Service;

import java.lang.reflect.Field;
import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.Investor;
import com.devcamp.home24h.Repository.InvestorRepository;

@Service
public class InvestorService {
    @Autowired
    InvestorRepository iInvestor;

    // Lấy ra tất cả investor
    public Page<Investor> getAllByPageAndSize(String keyword, String sortBy, String sortType, int page, int size) {
        String filled = getColumnName(sortBy);
        if(filled == null) {
            filled = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filled);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iInvestor.findAllByKeyword(keyword, pageable);
    }

    // Tạo mới 1 investor
    public Investor createInvestor(Investor pInvestor) {
        Investor investorCreated = iInvestor.save(pInvestor);
        return investorCreated;
    }

    // update 1 investor
    public Investor updateInvestor(Investor pInvestor,  int id) {
        Investor vInvestor = iInvestor.findById(id).get();
        vInvestor.setAddress(pInvestor.getAddress());
        vInvestor.setDescription(pInvestor.getDescription());
        vInvestor.setEmail(pInvestor.getEmail());
        vInvestor.setFax(pInvestor.getFax());
        vInvestor.setNote(pInvestor.getNote());
        vInvestor.setName(pInvestor.getName());
        vInvestor.setPhone(pInvestor.getPhone());
        vInvestor.setPhone2(pInvestor.getPhone2());
        vInvestor.setProjects(pInvestor.getProjects());
        vInvestor.setWebsite(pInvestor.getWebsite());
        Investor investorUpdated = iInvestor.save(vInvestor);
        return investorUpdated;
    }

    // delete investor
    public void deleteInvestor(int id) {
        iInvestor.deleteById(id);
    }

    public String getColumnName(String fieldName) {
        try {
            Field field = Investor.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
