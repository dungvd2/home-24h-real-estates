package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer>{
    @Query(value = "SELECT * FROM province WHERE _name LIKE %:name%", nativeQuery = true)
    List<Province> filterProvincesByName(@Param("name") String name);

    @Query(value = "SELECT * FROM province " + 
        "WHERE _name LIKE %:keyword% " +
        "OR _code LIKE %:keyword% ", nativeQuery = true)
    Page<Province> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
