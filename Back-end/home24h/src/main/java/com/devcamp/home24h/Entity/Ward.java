package com.devcamp.home24h.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ward")
public class Ward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Vui lòng nhập tên xã")
    @Column(name = "_name", nullable = false)
    private String name;

    @Column(name = "_prefix")
    private String prefix;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "_district_id")
    @JsonIgnore
    private District district;

    @Column(name = "_province_id")
    private int provinceId;

    @Transient
    private String districtName;

    @Transient
    private String provinceName;

    public Ward() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }


    public int getProvinceId() {
        return this.district.getProvinceId();
    }

    public String getDistrictName() {
        return this.district.getPrefix() + " " + this.district.getName();
    }
    
    public String getProvinceName() {
        return this.district.getProvinceName();
    }

    public int getDistrictId() {
        return this.district.getId();
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    
}
