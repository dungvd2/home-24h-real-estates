package com.devcamp.home24h.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.Location;
import com.devcamp.home24h.Repository.LocationRepository;

@Service
public class LocationService {
    @Autowired
    LocationRepository iLocation;

    // Get all
    public List<Location> getAll() {
        return iLocation.findAll();
    }

    // hàm tạo mới 1 location
    public Location createLocation(Location pLocation) {
        Location createdLocation = iLocation.save(pLocation);
        return createdLocation;
    }

    // Hàm cập nhật 1 location
    public Location updateLocation(Location pLocation, int id) {
        Location vLocation = iLocation.findById(id).get();
        vLocation.setLatitude(pLocation.getLatitude());
        vLocation.setLongitude(pLocation.getLongitude());
        Location locationUpdated = iLocation.save(vLocation);
        return locationUpdated;
    }

    // Hàm xóa 1 location
    public void deleteLocation(int id) {
        iLocation.deleteById(id);
    }
} 
