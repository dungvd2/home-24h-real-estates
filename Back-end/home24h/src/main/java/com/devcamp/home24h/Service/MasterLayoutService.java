package com.devcamp.home24h.Service;

import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.MasterLayout;
import com.devcamp.home24h.Entity.Project;
import com.devcamp.home24h.Repository.MasterLayoutRepository;
import com.devcamp.home24h.Repository.ProjectRepository;

@Service
public class MasterLayoutService {
    @Autowired
    MasterLayoutRepository iMasterLayout;

    @Autowired
    ProjectRepository iProject;
    // Lấy ra toàn bộ master layout
    public Page<MasterLayout> getMasterLayoutsByPage(int page, int size, String keyword, String sortBy, String sortType) {
        String filled = getColumnName(sortBy);
        if(filled == null) {
            filled = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filled);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iMasterLayout.findAllByKeyword(keyword, pageable);
    }

    // APi để tạo mới 1 master-layout
    public MasterLayout createNewMasterLayout(MasterLayout pMasterLayout) {
        Project project = iProject.findById(pMasterLayout.getProjectId()).get();
        pMasterLayout.setProjectName(project.getName() + ", " + project.getNumApartment());
        Date newDate = new Date();
        pMasterLayout.setDateCreate(newDate);
        MasterLayout masterLayoutCreated =  iMasterLayout.save(pMasterLayout);
        return masterLayoutCreated;
    }

    // API để sửa thông tin một master-layout
    public MasterLayout updateMasterLayout(MasterLayout pMasterLayout, Integer mtId) {
        MasterLayout masterLayout = iMasterLayout.findById(mtId).get();
        Project project = iProject.findById(pMasterLayout.getProjectId()).get();
        masterLayout.setProjectId(pMasterLayout.getProjectId());
        masterLayout.setProjectName(project.getName() + ", " + project.getNumApartment());
        masterLayout.setAcreage(pMasterLayout.getAcreage());
        masterLayout.setApartmentList(pMasterLayout.getApartmentList());
        masterLayout.setDescription(pMasterLayout.getDescription());
        Date newDate = new Date();
        masterLayout.setDateUpdate(newDate);
        masterLayout.setPhoto(pMasterLayout.getPhoto());
        MasterLayout masterLayoutUpdated =  iMasterLayout.save(masterLayout);
        return masterLayoutUpdated;
    }

    // delete by id
    public void deleteMasterLayout(int id) {
        iMasterLayout.deleteById(id);
    }

    public String getColumnName(String fieldName) {
        try {
            Field field = MasterLayout.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
