package com.devcamp.home24h.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.User;

@Service
public class PasswordResetService {
    
    // Hàm kiểm tra xem pass code đã hết hạn chưa
    // Input: Thông tin user
    // Output: Lấy thời gian hiện tại so sánh với thời gian hết hạn và trả ra kết quả
    public Boolean isPassCodeExpired(User user) {
        if(user != null && user.getPassCodeExpiredTime() != null) {
            LocalDateTime currentTimestamp = LocalDateTime.now();
            LocalDateTime passCodeExpiredTime = user.getPassCodeExpiredTime();
            return currentTimestamp.isAfter(passCodeExpiredTime);
        }

        return true;
    }

    // Hàm kiểm tra xem passCode có còn hợp lệ nữa hay không
    // Input: Đối tượng user, pass code đươc user nhập vào
    // Output: True nếu hợp lệ và ngược lại
    public Boolean isPassCodeValid(User user, String inputPassCode) {
        if(isPassCodeExpired(user)) {
            return false;
        }
        return inputPassCode.equals(user.getPassCode());
    }

    // Hàm lấy ra thời gian hết hạn cho passcode (5 phút kể từ thời điểm tạo)
    public LocalDateTime getPassCodeExpiredTime() {
        LocalDateTime currentTimestamp = LocalDateTime.now();
        LocalDateTime expiryTime = currentTimestamp.plus(5, ChronoUnit.MINUTES);
        return expiryTime;
    }

}
