package com.devcamp.home24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.home24h.Entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{
}
