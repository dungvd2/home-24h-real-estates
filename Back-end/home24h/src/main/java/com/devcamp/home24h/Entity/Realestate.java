package com.devcamp.home24h.Entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "realestate")
public class Realestate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotEmpty(message = "Tiêu đề bài viết không được bỏ trống")
    @Column(name = "title", length = 2000)
    private String title;

    @NotNull(message = "Kiểu bất động sản phải được thêm vào")
    @Min(value = 0, message = "Kiểu bất động sản phải là số nguyên lớn hơn 0 và nhỏ hơn 4 theo quy ước")
    @Max(value = 4, message = "Kiểu bất động sản phải là số nguyên lớn hơn 0 và nhỏ hơn 4 theo quy ước")
    @Column(name = "type")
    private Integer type;
    
    @NotNull(message = "Nhu cầu cần được thêm vào")
    @Min(value = 0, message = "Nhu cầu phải là số nguyên lớn hơn 0 và nhỏ hơn 1 theo quy ước")
    @Max(value = 1, message = "Nhu cầu phải là số nguyên lớn hơn 0 và nhỏ hơn 1 theo quy ước")
    @Column(name = "request")
    private Integer request;

    @NotNull(message = "Tỉnh thành không được bỏ trống")
    @Positive(message = "Số thứ tự tỉnh thành phải là số nguyên lớn hơn 0")
    @Column(name = "province_id")
    private Integer provinceId;

    @NotNull(message = "Quận, huyện không được bỏ trống")
    @Positive(message = "Số thứ tự quận huyện phải là số nguyên lớn hơn 0")
    @Column(name = "district_id")
    private Integer districtId;

    @NotNull(message = "Phường, xã không được bỏ trống")
    @Positive(message = "Số thứ tự phường xã phải là số nguyên lớn hơn 0")
    @Column(name = "wards_id")
    private Integer wardsId;

    @Positive(message = "Số thứ tự đường phố phải là số nguyên lớn hơn 0")
    @Column(name = "street_id")
    private Integer streetId;

    @Column(name = "project_id")
    @Positive(message = "Dự án phải là số nguyên lớn hơn 0")
    private Integer projectId;

    @Column(name = "address", length = 2000)
    private String address;

    @NotNull(message = "Khách hàng không được bỏ trống")
    @Column(name = "customer_id")
    private Long customerId;

    @Positive(message = "Giá tiền phải lớn hơn 0đ")
    @Column(name = "price")
    private Long price;

    @Column(name = "price_time")
    private Integer priceTime;

    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "date_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @NotNull(message = "Vui lòng nhập diện tích bất động sản")
    @Positive(message = "Diện tích phải lớn hơn 0")
    @Column(name = "acreage")
    private BigDecimal acreage;

    @Column(name = "direction")
    @Min(value = 0, message = "Hướng phải là số nguyên lớn hơn 0 nhỏ hơn 8 theo quy ước")
    @Max(value = 8, message = "Hướng phải là số nguyên lớn hơn 0 nhỏ hơn 8 theo quy ước")
    private Integer direction;

    @Column(name = "total_floors")
    @PositiveOrZero(message = "Số tầng phải là số nguyên lớn hơn hoặc bằng 0")
    private Integer totalFloors;

    @Column(name = "number_floors")
    @PositiveOrZero(message = "Số tầng phải là số nguyên lớn hơn hoặc bằng 0")
    private Integer numberFloors;

    @Column(name = "bath")
    @PositiveOrZero(message = "Số phòng tắm là số nguyên lớn hơn hoặc bằng 0")
    private Integer bath;

    @Column(name = "bedroom")
    @PositiveOrZero(message = "Số phòng ngủ phải là số nguyên lớn hơn hoặc bằng 0")
    private Integer bedroom;

    @Column(name = "balcony")
    @PositiveOrZero(message = "Ban công phải là số nguyên lớn hơn hoặc bằng 0")
    private Integer balcony;

    @Column(name = "furniture_type")
    @Min(value = 0, message = "Kiểu nội thất là số nguyên lớn hơn 0 nhỏ hơn 2 theo quy ước")
    @Max(value = 2, message = "Kiểu nội thất là số nguyên lớn hơn 0 nhỏ hơn 2 theo quy ước")
    private Integer furnitureType;

    @Column(name = "price_rent")
    @Positive(message = "Giá cho thuê phải lớn hơn 0đ")
    private Long priceRent;

    @Column(name = "legal_doc")
    private String legalDoc;

    @Column(name = "description", length = 2000)
    @NotEmpty(message = "Mô tả bất động sản không được bỏ trống")
    private String description;

    @Column(name = "width_y")
    @Positive(message = "Chiều rộng phải lớn hơn 0 m")
    private Double widthY;

    @Column(name = "long_x")
    @Positive(message = "Chiều dài phải lớn hơn 0 m")
    private Double longX;

    @Column(name = "create_by")
    private Integer createBy;

    @Column(name = "update_by")
    private Integer updateBy;

    @Column(name = "photo", length = 2000)
    @NotEmpty(message = "Phải có ít nhất 1 hình ảnh của bất động sản được thêm vào")
    private String photo;

    @Column(name = "facade")
    @Positive(message = "Mặt tiền phải lớn hơn 0 m")
    private Double facade;

    @Column(name = "road_width")
    @Positive(message = "Độ rộng đường vào phải lớn hơn 0 m")
    private Double roadWidth;

    private Integer status;
    
    @Transient
    private String provinceName;

    @Transient
    private String districtName;
     
    @Transient
    private String wardName;

    @Transient
    private String streetName;

    @Transient
    private String customerName;

    @Transient 
    private String email;

    @Transient
    private String phoneNumber;

    @Transient
    private String projectName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardsId() {
        return wardsId;
    }

    public void setWardsId(Integer wardsId) {
        this.wardsId = wardsId;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Integer priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getTotalFloors() {
        return totalFloors;
    }

    public void setTotalFloors(Integer totalFloors) {
        this.totalFloors = totalFloors;
    }

    public Integer getNumberFloors() {
        return numberFloors;
    }

    public void setNumberFloors(Integer numberFloors) {
        this.numberFloors = numberFloors;
    }

    public Integer getBath() {
        return bath;
    }

    public void setBath(Integer bath) {
        this.bath = bath;
    }

    public Integer getBedroom() {
        return bedroom;
    }

    public void setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
    }

    public Integer getBalcony() {
        return balcony;
    }

    public void setBalcony(Integer balcony) {
        this.balcony = balcony;
    }

    public Integer getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(Integer furnitureType) {
        this.furnitureType = furnitureType;
    }

    public Long getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(Long priceRent) {
        this.priceRent = priceRent;
    }

    public String getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(String legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getWidthY() {
        return widthY;
    }

    public void setWidthY(Double widthY) {
        this.widthY = widthY;
    }

    public Double getLongX() {
        return longX;
    }

    public void setLongX(Double longX) {
        this.longX = longX;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getWardName() {
        return wardName;
    }

    public void setWardName(String wardName) {
        this.wardName = wardName;
    }

    public Double getFacade() {
        return facade;
    }

    public void setFacade(Double facade) {
        this.facade = facade;
    }

    public Double getRoadWidth() {
        return roadWidth;
    }

    public void setRoadWidth(Double roadWidth) {
        this.roadWidth = roadWidth;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    
}
