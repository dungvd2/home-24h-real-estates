package com.devcamp.home24h.Controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.MasterLayout;
import com.devcamp.home24h.Repository.MasterLayoutRepository;
import com.devcamp.home24h.Service.MasterLayoutService;

@RestController
@CrossOrigin
public class MasterLayoutController {
    @Autowired
    MasterLayoutService masterLayoutService;

    @Autowired
    MasterLayoutRepository iMasterLayout;

    
    @GetMapping("/master-layouts")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Page<MasterLayout>> getMTByPageAndSize(@RequestParam(defaultValue = "0") int page,
                                                                 @RequestParam(defaultValue = "10") int size,
                                                                 @RequestParam(defaultValue = "") String keyword,
                                                                 @RequestParam(defaultValue = "id") String sortBy,
                                                                 @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<MasterLayout> masterLayouts = masterLayoutService.getMasterLayoutsByPage(page, size, keyword, sortBy, sortType);
            return new ResponseEntity<>(masterLayouts, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get master layout by id
    @GetMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iMasterLayout.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để tạo mới 1 master layout
    @PostMapping("/master-layouts/")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<MasterLayout> createNewMasterLayout(@Valid @RequestBody MasterLayout pMasterLayout) {
        try {
            MasterLayout masterLayout = masterLayoutService.createNewMasterLayout(pMasterLayout);
            return new ResponseEntity<>(masterLayout, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API để sửa 1 master layout
    @PutMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<MasterLayout> updateMasterLayout(@Valid @RequestBody MasterLayout pMasterLayout, @PathVariable("id") Integer mtId) {
        try {
            MasterLayout masterLayout = masterLayoutService.updateMasterLayout(pMasterLayout, mtId);
            return new ResponseEntity<>(masterLayout, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<MasterLayout> deleteMasterLayout(@PathVariable("id") int id) {
        try {
            masterLayoutService.deleteMasterLayout(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

