package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.Realestate;

public interface RealestateRepository extends JpaRepository<Realestate, Integer> {
        // Lọc bằng kiểu, loại tin, trạng thái, vùng giá
        @Query(value = "SELECT * FROM realestate WHERE (:type = '' OR type LIKE %:type%) " +
                        "AND (:request = '' OR request LIKE %:request%) " +
                        "AND (:status = '' OR status LIKE %:status%) " +
                        "AND (:customerId = '-1' OR customer_id = :customerId) " +
                        "AND (:projectId = '-1' OR project_id = :projectId) " +
                        "AND (COALESCE(price, price_rent) BETWEEN :min AND :max)", nativeQuery = true)
        Page<Realestate> filterRealEstateByTypeRequestStatusAndPrice(
                        @Param("type") String type,
                        @Param("request") String request,
                        @Param("status") String status,
                        @Param("customerId") int customerId,
                        @Param("projectId") int projectId,
                        @Param("min") Long min,
                        @Param("max") Long max,
                        Pageable pageable);

        // Lọc dữ liệu bằng tỉnh, kiểu, loại tin, trạng thái, vùng giá có kiểu sắp xếp
        @Query(value = "SELECT * FROM realestate WHERE `province_id` = :provinceId AND (:type = '' OR type LIKE %:type%) " +
                        "AND (:request = '' OR request LIKE %:request%) " +
                        "AND (:status = '' OR status LIKE %:status%) " +
                        "AND (:customerId = '-1' OR customer_id = :customerId) " +
                        "AND (:projectId = '-1' OR project_id = :projectId) " +
                        "AND (COALESCE(price, price_rent) BETWEEN :min AND :max)", nativeQuery = true)
        Page<Realestate> filterRealEstateByProvinceTypeRequestStatusAndPrice(
                        @Param("provinceId") int provinceId,
                        @Param("type") String type,
                        @Param("request") String request,
                        @Param("status") String status,
                        @Param("customerId") int customerId,
                        @Param("projectId") int projectId,
                        @Param("min") Long min,
                        @Param("max") Long max,
                        Pageable pageable);

        // Lọc dữ liệu bằng huyện, kiểu, loại tin, trạng thái, vùng giá có kiểu sắp xếp
        @Query(value = "SELECT * FROM realestate WHERE district_id = :districtId AND (:type = '' OR type LIKE %:type%) "
                        +
                        "AND (:request = '' OR request LIKE %:request%) " +
                        "AND (:status = '' OR status LIKE %:status%) " +
                        "AND (:customerId = '-1' OR customer_id = :customerId) " +
                        "AND (:projectId = '-1' OR project_id = :projectId) " +
                        "AND (COALESCE(price, price_rent) BETWEEN :min AND :max)", nativeQuery = true)
        Page<Realestate> filterRealEstateByDistrictTypeRequestStatusAndPrice(
                        @Param("districtId") int districtId,
                        @Param("type") String type,
                        @Param("request") String request,
                        @Param("status") String status,
                        @Param("customerId") int customerId,
                        @Param("projectId") int projectId,
                        @Param("min") Long min,
                        @Param("max") Long max,
                        Pageable pageable);

        // Lọc dữ liệu bằng xã, kiểu, loại tin, trạng thái, vùng giá có kiểu sắp xếp
        @Query(value = "SELECT * FROM realestate WHERE wards_id = :wardId AND (:type = '' OR type LIKE %:type%) " +
                        "AND (:request = '' OR request LIKE %:request%) " +
                        "AND (:status = '' OR status LIKE %:status%) " +
                        "AND (:customerId = '-1' OR customer_id = :customerId) " +
                        "AND (:projectId = '-1' OR project_id = :projectId) " +
                        "AND (COALESCE(price, price_rent) BETWEEN :min AND :max)", nativeQuery = true)
        Page<Realestate> filterRealEstateByWardTypeRequestStatusAndPrice(
                        @Param("wardId") int wardId,
                        @Param("type") String type,
                        @Param("request") String request,
                        @Param("status") String status,
                        @Param("customerId") int customerId,
                        @Param("projectId") int projectId,
                        @Param("min") Long min,
                        @Param("max") Long max,
                        Pageable pageable);

        // Hàm đếm số lượng bất động sản có project id =
        Long countByProjectId(Integer projectId);

        // Hàm load ra 9 bất động sản mới đăng gần nhất, và được duyệt
        @Query(value = "SELECT * FROM realestate WHERE status = 1 ORDER BY date_create DESC LIMIT 6", nativeQuery = true)
        List<Realestate> getAllRecentRealestate();

        // Hàm load ra 9 bất động sản để bán mới đăng gần nhất, và được duyệt
        @Query(value = "SELECT * FROM realestate WHERE status = 1 AND request = 0 ORDER BY date_create DESC LIMIT 6", nativeQuery = true)
        List<Realestate> getAllRecentRealestateForSale();

        // Hàm load ra 9 bất động sản cho thuê mới đăng gần nhất, và được duyệt
        @Query(value = "SELECT * FROM realestate WHERE status = 1 AND request = 1 ORDER BY date_create DESC LIMIT 6", nativeQuery = true)
        List<Realestate> getAllRecentRealestateForRent();

        // Hàm lấy ra danh sách các bất động sản phù hợp với tiêu chí sắp xếp theo giá tiền sẽ quy ước là giá thuê + giá mua
        @Query(value =  "SELECT * FROM realestate WHERE ((:ward = '' OR wards_id = :ward) " + 
                        "AND (:district = '' OR district_id = :district) " +
                        "AND (:province = '' OR province_id = :province)) " +
                        "AND (:request = '' OR request = :request) "+ 
                        "AND (:type = '' OR type = :type) " + 
                        "AND (COALESCE(price, price_rent) BETWEEN :pricemin AND :pricemax) " + 
                        "AND (acreage BETWEEN :areamin AND :areamax) " +
                        "AND (COALESCE(bedroom, 0) BETWEEN :bedroommin AND :bedroommax) " +
                        "AND (COALESCE(bath,0) BETWEEN :bathroommin AND :bathroommax) " + 
                        "AND (:direction = '' OR direction =:direction) " +
                        "AND status = 1 " +
                        "ORDER BY "+
                        "CASE WHEN :sortType = 'asc' THEN (COALESCE(price,0) + COALESCE(price_rent, 0)) END ASC, " +
                        "CASE WHEN :sortType = 'desc' THEN (COALESCE(price,0) + COALESCE(price_rent, 0)) END DESC", nativeQuery = true)
        Page<Realestate> searchRealesateForCustomer(@Param("province") String province,
                                                    @Param("district") String district,
                                                    @Param("ward") String ward,
                                                    @Param("request") String request,
                                                    @Param("type") String type,
                                                    @Param("pricemin") Long pricemin,
                                                    @Param("pricemax") Long pricemax,
                                                    @Param("areamin") Double areamin,
                                                    @Param("areamax") Double areamax,
                                                    @Param("bedroommin") int bedroommin,
                                                    @Param("bedroommax") int bedroommax,
                                                    @Param("bathroommin") int bathroommin,
                                                    @Param("bathroommax") int bathroommax,
                                                    @Param("direction") String direction,
                                                    @Param("sortType") String sortType,
                                                    Pageable pageable);


        // Hàm lấy ra danh sách các bất động sản phù hợp với tiêu chí lọc không sắp xếp
        @Query(value =  "SELECT * FROM realestate WHERE ((:ward = '' OR wards_id = :ward) " + 
                        "AND (:district = '' OR district_id = :district) " +
                        "AND (:province = '' OR province_id = :province)) " +
                        "AND (:request = '' OR request = :request) "+ 
                        "AND (:type = '' OR type = :type) " + 
                        "AND (COALESCE(price, price_rent) BETWEEN :pricemin AND :pricemax) " + 
                        "AND (acreage BETWEEN :areamin AND :areamax) " +
                        "AND (COALESCE(bedroom, 0) BETWEEN :bedroommin AND :bedroommax) " +
                        "AND (COALESCE(bath,0) BETWEEN :bathroommin AND :bathroommax) " + 
                        "AND status = 1 " +
                        "AND (:direction = '' OR direction =:direction) ", nativeQuery = true)
        Page<Realestate> searchRealesateForCustomerWithoutSort(@Param("province") String province,
                                                    @Param("district") String district,
                                                    @Param("ward") String ward,
                                                    @Param("request") String request,
                                                    @Param("type") String type,
                                                    @Param("pricemin") Long pricemin,
                                                    @Param("pricemax") Long pricemax,
                                                    @Param("areamin") Double areamin,
                                                    @Param("areamax") Double areamax,
                                                    @Param("bedroommin") int bedroommin,
                                                    @Param("bedroommax") int bedroommax,
                                                    @Param("bathroommin") int bathroommin,
                                                    @Param("bathroommax") int bathroommax,
                                                    @Param("direction") String direction,
                                                    Pageable pageable);

       // Hàm lấy ra danh sách các bất động sản phù hợp với tiêu chí lọc sắp xếp theo thời gian gần nhất
        @Query(value =  "SELECT * FROM realestate WHERE ((:ward = '' OR wards_id = :ward) " + 
                        "AND (:district = '' OR district_id = :district) " +
                        "AND (:province = '' OR province_id = :province)) " +
                        "AND (:request = '' OR request = :request) "+ 
                        "AND (:type = '' OR type = :type) " + 
                        "AND (COALESCE(price, price_rent) BETWEEN :pricemin AND :pricemax) " + 
                        "AND (acreage BETWEEN :areamin AND :areamax) " +
                        "AND (COALESCE(bedroom, 0) BETWEEN :bedroommin AND :bedroommax) " +
                        "AND (COALESCE(bath,0) BETWEEN :bathroommin AND :bathroommax) " + 
                        "AND (:direction = '' OR direction =:direction) " +
                        "AND status = 1 " +
                        "ORDER BY date_create DESC", nativeQuery = true)
        Page<Realestate> searchRealesateForCustomerSortByTimeCreate(@Param("province") String province,
                                                    @Param("district") String district,
                                                    @Param("ward") String ward,
                                                    @Param("request") String request,
                                                    @Param("type") String type,
                                                    @Param("pricemin") Long pricemin,
                                                    @Param("pricemax") Long pricemax,
                                                    @Param("areamin") Double areamin,
                                                    @Param("areamax") Double areamax,
                                                    @Param("bedroommin") int bedroommin,
                                                    @Param("bedroommax") int bedroommax,
                                                    @Param("bathroommin") int bathroommin,
                                                    @Param("bathroommax") int bathroommax,
                                                    @Param("direction") String direction,
                                                    Pageable pageable); 

        // Hàm lấy ra danh sách bất động sản của người dùng đã thêm không sắp xếp
        @Query(value =  "SELECT * FROM realestate WHERE customer_id = :customerId " + 
                        "AND (:status = '' OR status = :status)", nativeQuery = true)
        Page<Realestate> getListRealOfCustomer(@Param("customerId") Long customerId, 
                                               @Param("status") String status, Pageable pageable);

        // Hàm lấy ra danh sách bất động sản của người dùng đã thêm sắp xếp theo giá
        @Query(value =  "SELECT * FROM realestate WHERE customer_id = :customerId " + 
                        "AND (:status = '' OR status = :status) " +
                        "ORDER BY "+
                        "CASE WHEN :sortType = 'asc' THEN (COALESCE(price,0) + COALESCE(price_rent, 0)) END ASC, " +
                        "CASE WHEN :sortType = 'desc' THEN (COALESCE(price,0) + COALESCE(price_rent, 0)) END DESC", nativeQuery = true) 
        Page<Realestate> getListRealOfCustomerSortByPrice(@Param("customerId") Long customerId, 
                                                        @Param("status") String status,
                                                        @Param("sortType") String sortType, Pageable pageable);       
                                                        
        // Hàm lấy ra danh sách bất động sản của người dùng đã thêm sắp xếp theo thời gian gần nhất
        @Query(value =  "SELECT * FROM realestate WHERE customer_id = :customerId " + 
                        "AND (:status = '' OR status = :status) " +
                        "ORDER BY date_create DESC", nativeQuery = true)
        Page<Realestate> getListRealOfCustomerSortByTime(@Param("customerId") Long customerId, 
                                               @Param("status") String status, Pageable pageable);
}
