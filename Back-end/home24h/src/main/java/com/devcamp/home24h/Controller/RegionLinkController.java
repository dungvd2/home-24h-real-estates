package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.RegionLink;
import com.devcamp.home24h.Repository.RegionLinkRepository;
import com.devcamp.home24h.Service.RegionLinkService;

@RestController
@CrossOrigin
public class RegionLinkController {
    @Autowired
    private RegionLinkService regionLinkService;

    @Autowired RegionLinkRepository iRegionLink;

    @GetMapping("/regionlinks")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAllRegionLinks(@RequestParam(defaultValue = "0") int page, 
                                                    @RequestParam(defaultValue = "10") int size,
                                                    @RequestParam(defaultValue = "") String keyword,
                                                    @RequestParam(defaultValue = "id") String sortBy,
                                                    @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<RegionLink> regionLinks = regionLinkService.getAllRegionLinkByPage(page, size, keyword, sortBy, sortType);
            return new ResponseEntity<>(regionLinks, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regionlinks/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAll() {
        try {
            List<RegionLink> regionLinks = iRegionLink.findAll();
            return new ResponseEntity<>(regionLinks, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get region link by idư
    @GetMapping("/regionlinks/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<RegionLink> findRegionLinkById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iRegionLink.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/regionlinks")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<RegionLink> createRegionLink(@Valid @RequestBody RegionLink regionLink) {
        try {
            RegionLink createdRegionLink = regionLinkService.createRegionLink(regionLink);
            return new ResponseEntity<>(createdRegionLink, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @PutMapping("/regionlinks/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<RegionLink> updateRegionLink(@Valid @PathVariable("id") int id, @RequestBody RegionLink regionLink) {
        try {
            RegionLink updatedRegionLink = regionLinkService.updateRegionLink(regionLink, id);
            return new ResponseEntity<>(updatedRegionLink, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/regionlinks/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<HttpStatus> deleteRegionLink(@PathVariable("id") int id) {
        try {
            regionLinkService.deleteRegionLink(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

