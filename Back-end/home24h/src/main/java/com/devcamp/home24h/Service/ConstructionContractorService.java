package com.devcamp.home24h.Service;

import java.lang.reflect.Field;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.ConstructionContractor;
import com.devcamp.home24h.Repository.ConstructionContractorRepository;
import com.devcamp.home24h.Repository.ProjectRepository;


@Service
public class ConstructionContractorService {
    @Autowired
    ConstructionContractorRepository iConstructionContractor;

    @Autowired
    ProjectRepository iProject;

    // Hàm lấy ra toàn bộ Construction Contractor có phân trang
    public Page<ConstructionContractor> getAllConstructionContractorsByPage(String keyword, String sortBy, String sortType, int page, int size) {
        String filed = getColumnName(sortBy);
        if(filed == null) {
            filed = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filed);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iConstructionContractor.findAllByKeyword(keyword, pageable);
    }

    // Hàm tạo mới 1 construction contractor
    public ConstructionContractor createConstructionContractor(ConstructionContractor pConstructionContractor) {
        ConstructionContractor createdConstructionContractor = iConstructionContractor.save(pConstructionContractor);
        return createdConstructionContractor;
    }

    // Hàm sửa 1 construction contractor
    public ConstructionContractor updateConstructionContractor(ConstructionContractor pConstructionContractor, Integer id) {
        ConstructionContractor vConstructionContractor = iConstructionContractor.findById(id).get();
        vConstructionContractor.setAddress(pConstructionContractor.getAddress()); 
        vConstructionContractor.setDescription(pConstructionContractor.getDescription()); 
        vConstructionContractor.setEmail(pConstructionContractor.getEmail()); 
        vConstructionContractor.setName(pConstructionContractor.getName()); 
        vConstructionContractor.setPhone(pConstructionContractor.getPhone()); 
        vConstructionContractor.setPhone2(pConstructionContractor.getPhone2()); 
        vConstructionContractor.setNote(pConstructionContractor.getNote()); 
        vConstructionContractor.setProjects(pConstructionContractor.getProjects()); 
        vConstructionContractor.setWebsite(pConstructionContractor.getWebsite()); 
        vConstructionContractor.setFax(pConstructionContractor.getFax()); 
        ConstructionContractor vCCUpdated = iConstructionContractor.save(vConstructionContractor);
        return vCCUpdated;
    }

    // Hàm xóa 1 construction contractor
    public void deleteConstructionContractor(Integer id) {
        iProject.updateConstructionContractorToNull(id);
        iConstructionContractor.deleteById(id);
    }

    public String getColumnName(String fieldName) {
        try {
            Field field = ConstructionContractor.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
