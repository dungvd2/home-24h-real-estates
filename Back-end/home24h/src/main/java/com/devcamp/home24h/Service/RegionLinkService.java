package com.devcamp.home24h.Service;

import java.lang.reflect.Field;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.RegionLink;
import com.devcamp.home24h.Repository.RegionLinkRepository;

@Service
public class RegionLinkService {
    @Autowired
    RegionLinkRepository iRegionLink;

    @Autowired
    ProjectService projectService;

    // get all region Link
    public Page<RegionLink> getAllRegionLinkByPage(int page, int size, String keyword, String sortBy, String sortType) {
        String filled = getColumnName(sortBy);
        if(filled == null) {
            filled = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filled);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iRegionLink.findAllByKeyword(keyword, pageable);
    }

    // create region link
    public RegionLink createRegionLink(RegionLink pRegionLink) {
        RegionLink createdRegionLink = iRegionLink.save(pRegionLink);
        return createdRegionLink;
    }

    // update region link
    public RegionLink updateRegionLink(RegionLink pRegionLink, int id) {
        RegionLink vRegionLink = iRegionLink.findById(id).get();
        vRegionLink.setAddress(pRegionLink.getAddress());
        vRegionLink.setDescription(pRegionLink.getDescription());
        vRegionLink.setLatitude(pRegionLink.getLatitude());
        vRegionLink.setLongitude(pRegionLink.getLongitude());
        vRegionLink.setName(pRegionLink.getName());
        vRegionLink.setPhoto(pRegionLink.getPhoto());
        RegionLink regionLinkUpdated = iRegionLink.save(vRegionLink);
        return regionLinkUpdated;
    }

    // Delete by id
    public void deleteRegionLink(int id) {
        projectService.updateProjectWhenDeleteRegionLink(id);
        iRegionLink.deleteById(id);
    }

    // Hàm lấy ra tên column
    public String getColumnName(String fieldName) {
        try {
            Field field = RegionLink.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
