package com.devcamp.home24h.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.Utilities;

public interface UtilitiesRepository extends JpaRepository<Utilities, Integer>{
    @Query(value = "SELECT * FROM utilities WHERE " + 
        "name LIKE %:keyword% ", nativeQuery = true)
    Page<Utilities> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
