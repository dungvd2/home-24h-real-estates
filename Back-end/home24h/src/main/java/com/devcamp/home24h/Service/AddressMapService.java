package com.devcamp.home24h.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.AddressMap;
import com.devcamp.home24h.Repository.AddressMapRepository;

@Service
public class AddressMapService {
    @Autowired
    AddressMapRepository iAddressMap;

    // Lấy thông tin toàn bộ Address Map
    public List<AddressMap> getAllAddressMap() {
        List<AddressMap> allAddressMap = new ArrayList<>();
        iAddressMap.findAll().forEach(allAddressMap::add);
        return allAddressMap;
    }

    // Thêm mới 1 Address Map
    public AddressMap createAddressMap(AddressMap pAddressMap) {
        AddressMap addressMapCreated = iAddressMap.save(pAddressMap);
        return addressMapCreated;
    } 

    // Sửa 1 Address map
    public AddressMap updateAddressMap(AddressMap pAddressMap, Integer id) {
        AddressMap vAddressMap = iAddressMap.findById(id).get();
        vAddressMap.setAddress(pAddressMap.getAddress());
        vAddressMap.setLatitude(pAddressMap.getLatitude());
        vAddressMap.setLongitude(pAddressMap.getLongitude());
        AddressMap vAddressMapUpdated = iAddressMap.save(vAddressMap);
        return vAddressMapUpdated;
    }


    // Xóa 1 Address map
    public void deleteAddressMap(Integer id) {
        iAddressMap.deleteById(id);
    }

    // Lấy ra danh sách addressmap có phân trang
    // Lấy ra tên của cột trong db trước sau đó tạo 1 đối tượng sort và gán vào pageable
    public Page<AddressMap> getAddressMapByPage(int page, int size, String keyword, String sortBy, String sortType) {
        String filed = getColumnName(sortBy);
        if(filed == null) {
            filed = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filed);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iAddressMap.findAllByKeyword(keyword, pageable);
    }

    public String getColumnName(String fieldName) {
        try {
            Field field = AddressMap.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
