package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.home24h.Entity.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>{

    // Tìm danh sách project theo tên
    @Query(value = "SELECT * FROM project WHERE `_name` LIKE %:name%", nativeQuery = true)
    Page<Project> findByNameContainingIgnoreCase(String name, Pageable pageable);

    // Tìm theo tên và tỉnh thành
    @Query(value = "SELECT * FROM project WHERE `_province_id` = :provinceId AND `_name` LIKE %:name%", nativeQuery = true)
    Page<Project> findByNameAndProvinceId(String name, int provinceId, Pageable pageable);

    // Tìm theo tên và quận, huyện
    @Query(value = "SELECT * FROM project WHERE `_district_id` = :districtId AND `_name` LIKE %:name%", nativeQuery = true)
    Page<Project> findByNameAndDistrictId(String name, int districtId, Pageable pageable);

    // Lấy ra danh sách Project theo quận huyện
    List<Project> findByDistrictId(Integer districtId);

    // Lấy ra danh sách Project theo tỉnh
    List<Project> findByProvinceId(Integer provinceId);

    // Chuyển các project có số nhà thầu là id thành null (do đã xóa nhà thầu đó nên cần xử lý dữ liệu project kèm theo)
    @Transactional
    @Modifying
    @Query(value = "UPDATE project SET construction_contractor = NULL WHERE construction_contractor = :contractorId", nativeQuery = true)
    void updateConstructionContractorToNull(@Param("contractorId") Integer contractorId);

    // Chuyển các project có số đơn vị thiết kế là id thành null (do đã xóa đơn vị thiết kế)
    @Transactional
    @Modifying
    @Query(value = "UPDATE project SET design_unit = NULL WHERE design_unit = :designUnitId", nativeQuery = true)
    void updateDesignUnitToNull(@Param("designUnitId") Integer designUnitId);


    // Đếm số lượng dự án có nhà đầu tư theo id
    Long countByInvestor(Integer investor);

    // Hàm lấy ra danh sách các project có vùng kết nối id là 
    @Query(value = "SELECT * FROM project WHERE region_link LIKE %:regionLinkId%", nativeQuery = true)
    List<Project> getAllProjectHasRegionLink(@Param("regionLinkId") Integer regionLinkId);

    // Hàm lấy ra danh sách các project có utitlites là 
    @Query(value = "SELECT * FROM project WHERE utilities LIKE %:utilitiesId%", nativeQuery = true)
    List<Project> getAllProjectHasUtilities(@Param("utilitiesId") Integer utilitiesId);

}
