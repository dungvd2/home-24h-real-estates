package com.devcamp.home24h.Service;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.Customer;
import com.devcamp.home24h.Repository.CustomerRepository;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository iCustomer;

    // Hàm lấy toàn bộ thông tin customer
    public Page<Customer> filterCustomers(String name, String phone, String type, int page, int size) {
        List<Customer> listAll = iCustomer.filterCustomers(name, phone, type);
        Pageable pageable = PageRequest.of(page, size);
        int start = page * size;
        int end = Math.min(start + size, listAll.size());
        Page<Customer> customersPagination = new PageImpl<>(listAll.subList(start, end), pageable, listAll.size());
        return customersPagination;
    }

    // Hàm tạo mới 1 customer
    public Customer createCustomer(Customer pCustomer) {
        Date vDate = new Date();
        pCustomer.setCreateDate(vDate);
        Customer customerCreated = iCustomer.save(pCustomer);
        return customerCreated;
    }

    // Hàm sửa 1 customer
    public Customer updateCustomer(Customer pCustomer, Integer id) {
        Date vDate = new Date();
        Customer vCustomer = iCustomer.findById(id).get();
        vCustomer.setUpdateDate(vDate);
        vCustomer.setAddress(pCustomer.getAddress());
        vCustomer.setContactName(pCustomer.getContactName());
        vCustomer.setContactTitle(pCustomer.getContactTitle());
        vCustomer.setEmail(pCustomer.getEmail());
        vCustomer.setNote(pCustomer.getNote());
        vCustomer.setMobile(pCustomer.getMobile());
        Customer updatedCustomer = iCustomer.save(vCustomer);
        return updatedCustomer;
    }

    // Hàm xóa 1 customer
    public void deleteCustomer(Integer id) {
        iCustomer.deleteById(id);
    }
}
