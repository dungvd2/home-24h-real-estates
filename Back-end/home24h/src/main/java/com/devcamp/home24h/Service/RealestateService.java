package com.devcamp.home24h.Service;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.District;
import com.devcamp.home24h.Entity.Project;
import com.devcamp.home24h.Entity.Province;
import com.devcamp.home24h.Entity.Realestate;
import com.devcamp.home24h.Entity.Street;
import com.devcamp.home24h.Entity.User;
import com.devcamp.home24h.Entity.Ward;
import com.devcamp.home24h.Repository.DistrictRepository;
import com.devcamp.home24h.Repository.ProjectRepository;
import com.devcamp.home24h.Repository.ProvinceRepository;
import com.devcamp.home24h.Repository.RealestateRepository;
import com.devcamp.home24h.Repository.StreetRepository;
import com.devcamp.home24h.Repository.UserRepository;
import com.devcamp.home24h.Repository.WardRepository;

@Service
public class RealestateService {
    @Autowired
    RealestateRepository realEstateRepository;

    @Autowired 
    private ProvinceRepository iProvince;

    @Autowired 
    private DistrictRepository iDistrict;

    @Autowired
    private WardRepository iWard;

    @Autowired
    private StreetRepository iStreet;

    @Autowired
    private UserRepository iUser;

    @Autowired
    private ProjectRepository iProject;

    public List<Realestate> getAll() {
        return realEstateRepository.findAll();
    }

    // Hàm lọc dữ liệu bất động sản ở trang admin
    public Page<Realestate> filterRealEstateAdminPage(String status, String request, 
                                             String type, int provinceId, 
                                             int districtId, int wardId,
                                             int customerId, int projectId,
                                             Long minPrice, Long maxPrice,
                                             String sortBy, String sortType,
                                             int page, int size) {
        String filed = this.getColumnName(sortBy);
        if(filed == null) {
            filed = "id";
            sortType = "desc";
        }
        if(filed.equals("id")) {
            sortType = "desc";
        }
        // Vì bảng giá gộp về 1 cột giữa thuê và giá bán cho nên chúng ta phải xét trường hợp nếu tăng dần thì tính giá thuê tiêu chí phụ là giá mua
        Sort sort;
        if(filed.equals("price") && sortType.equals("asc")) {
            sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), "price_rent");
            sort = sort.and(Sort.by(Sort.Direction.fromString(sortType), "price"));
        }
        // Nếu giảm dần thì xếp theo giá bán và tiêu chí phụ là giá thuê thông qua hàm sort.end
        else if(filed.equals("price") && sortType.equals("desc")) {
            sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), "price");
            sort = sort.and(Sort.by(Sort.Direction.fromString(sortType), "price_rent"));
        }
        // Nếu ở các ô khác thì chỉ cần điền vào giá trị đó không cần tiêu chí phụ
        else {
            sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filed);
        }

        Pageable pageable = PageRequest.of(page, size, sort);
        if(provinceId == 0) {
            return realEstateRepository.filterRealEstateByTypeRequestStatusAndPrice(type, request, status, customerId, projectId, minPrice, maxPrice, pageable);
        }
        else if(districtId == 0) {
            return realEstateRepository.filterRealEstateByProvinceTypeRequestStatusAndPrice(provinceId, type, request, status, customerId, projectId, minPrice, maxPrice, pageable);
        }
        else if(wardId == 0) {
            return realEstateRepository.filterRealEstateByDistrictTypeRequestStatusAndPrice(districtId, type, request, status, customerId, projectId, minPrice, maxPrice, pageable);
        }
        else {
            return realEstateRepository.filterRealEstateByWardTypeRequestStatusAndPrice(wardId, type, request, status, customerId, projectId, minPrice, maxPrice, pageable);
        }
    }

    // Hàm lọc dữ liệu bất động sản ở trang khách hàng
    public Page<Realestate> searchRealEstateCustomerPage(String province, String district, String ward, String request, String type, Long pricemin, 
                                                        Long priceMax, double areamin, double areamax, int bedroommin, 
                                                        int bedroommax, int bathroommin, int bathroommax, 
                                                        String direction, String sortBy, String sortType, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        if(sortBy.equals("price")) {
            return realEstateRepository.searchRealesateForCustomer(province, district, ward, request, type, pricemin, priceMax, areamin, areamax, bedroommin, bedroommax, bathroommin, bathroommax, direction, sortType, pageable);
        }        
        else if(sortBy.equals("date")) {
            return realEstateRepository.searchRealesateForCustomerSortByTimeCreate(province,district, ward, request, type, pricemin, priceMax, areamin, areamax, bedroommin, bedroommax, bathroommin, bathroommax, direction, pageable);
        }              
        else {
            return realEstateRepository.searchRealesateForCustomerWithoutSort(province,district, ward, request, type, pricemin, priceMax, areamin, areamax, bedroommin, bedroommax, bathroommin, bathroommax, direction, pageable);
        }                              
    }

    // Hàm lấy ra danh sách dữ liệu bất động sản mà người dùng đã đăng theo các tiêu chí
    public Page<Realestate> getListRealestateCreateByUser(Long customerId, String status, String sortBy, String sortType, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        if(sortBy.equals("price")) {
            return realEstateRepository.getListRealOfCustomerSortByPrice(customerId, status, sortType, pageable);
        }
        else if(sortBy.equals("date")) {
            return realEstateRepository.getListRealOfCustomerSortByTime(customerId, status, pageable);
        }
        else {
            return realEstateRepository.getListRealOfCustomer(customerId, status, pageable);
        }
    }

    // Create a new real estate
    public Realestate createRealEstate(Realestate realEstate) {
        Date newDate = new Date();
        realEstate.setDateCreate(newDate);
        realEstate.setStatus(1);
        return realEstateRepository.save(realEstate);
    }

    // Tạo mới bất động sản bởi khách hàng
    public Realestate createRealEstateByCustomer(Realestate realEstate, Long userId) {
        Date newDate = new Date();
        realEstate.setDateCreate(newDate);
        realEstate.setStatus(0);
        realEstate.setCustomerId(userId);
        return realEstateRepository.save(realEstate);
    }

    // Update a real estate
    public Realestate updateRealEstate(Realestate realEstate, int id) {
        Realestate existingRealEstate = realEstateRepository.findById(id).get();
        Date newDate = new Date();
        existingRealEstate.setTitle(realEstate.getTitle());
        existingRealEstate.setType(realEstate.getType());
        existingRealEstate.setRequest(realEstate.getRequest());
        existingRealEstate.setProvinceId(realEstate.getProvinceId());
        existingRealEstate.setDistrictId(realEstate.getDistrictId());
        existingRealEstate.setWardsId(realEstate.getWardsId());
        existingRealEstate.setStreetId(realEstate.getStreetId());
        existingRealEstate.setProjectId(realEstate.getProjectId());
        existingRealEstate.setAddress(realEstate.getAddress());
        existingRealEstate.setCustomerId(realEstate.getCustomerId());
        existingRealEstate.setPrice(realEstate.getPrice());
        existingRealEstate.setPriceTime(realEstate.getPriceTime());
        existingRealEstate.setDateUpdate(newDate);
        existingRealEstate.setAcreage(realEstate.getAcreage());
        existingRealEstate.setDirection(realEstate.getDirection());
        existingRealEstate.setTotalFloors(realEstate.getTotalFloors());
        existingRealEstate.setNumberFloors(realEstate.getNumberFloors());
        existingRealEstate.setBath(realEstate.getBath());
        existingRealEstate.setBedroom(realEstate.getBedroom());
        existingRealEstate.setBalcony(realEstate.getBalcony());
        existingRealEstate.setFurnitureType(realEstate.getFurnitureType());
        existingRealEstate.setPriceRent(realEstate.getPriceRent());
        existingRealEstate.setLegalDoc(realEstate.getLegalDoc());
        existingRealEstate.setDescription(realEstate.getDescription());
        existingRealEstate.setWidthY(realEstate.getWidthY());
        existingRealEstate.setLongX(realEstate.getLongX());
        existingRealEstate.setCreateBy(realEstate.getCreateBy());
        existingRealEstate.setUpdateBy(realEstate.getUpdateBy());
        existingRealEstate.setPhoto(realEstate.getPhoto());
        existingRealEstate.setRoadWidth(realEstate.getRoadWidth());
        existingRealEstate.setFacade(realEstate.getFacade());
        return realEstateRepository.save(existingRealEstate);
    }

    // Hàm cập nhật bất động sản bởi khách hàng
    public Realestate updateRealEstateByCustomer(Realestate existingRealEstate, Realestate pRealEstate) {
        Date newDate = new Date();
        existingRealEstate.setTitle(pRealEstate.getTitle());
        existingRealEstate.setType(pRealEstate.getType());
        existingRealEstate.setRequest(pRealEstate.getRequest());
        existingRealEstate.setProvinceId(pRealEstate.getProvinceId());
        existingRealEstate.setDistrictId(pRealEstate.getDistrictId());
        existingRealEstate.setWardsId(pRealEstate.getWardsId());
        existingRealEstate.setStreetId(pRealEstate.getStreetId());
        existingRealEstate.setProjectId(pRealEstate.getProjectId());
        existingRealEstate.setAddress(pRealEstate.getAddress());
        existingRealEstate.setPrice(pRealEstate.getPrice());
        existingRealEstate.setPriceTime(pRealEstate.getPriceTime());
        existingRealEstate.setDateUpdate(newDate);
        existingRealEstate.setAcreage(pRealEstate.getAcreage());
        existingRealEstate.setDirection(pRealEstate.getDirection());
        existingRealEstate.setTotalFloors(pRealEstate.getTotalFloors());
        existingRealEstate.setNumberFloors(pRealEstate.getNumberFloors());
        existingRealEstate.setBath(pRealEstate.getBath());
        existingRealEstate.setBedroom(pRealEstate.getBedroom());
        existingRealEstate.setBalcony(pRealEstate.getBalcony());
        existingRealEstate.setFurnitureType(pRealEstate.getFurnitureType());
        existingRealEstate.setPriceRent(pRealEstate.getPriceRent());
        existingRealEstate.setLegalDoc(pRealEstate.getLegalDoc());
        existingRealEstate.setDescription(pRealEstate.getDescription());
        existingRealEstate.setWidthY(pRealEstate.getWidthY());
        existingRealEstate.setLongX(pRealEstate.getLongX());
        existingRealEstate.setCreateBy(pRealEstate.getCreateBy());
        existingRealEstate.setUpdateBy(pRealEstate.getUpdateBy());
        existingRealEstate.setPhoto(pRealEstate.getPhoto());
        existingRealEstate.setRoadWidth(pRealEstate.getRoadWidth());
        existingRealEstate.setFacade(pRealEstate.getFacade());
        existingRealEstate.setStatus(0);
        return realEstateRepository.save(existingRealEstate);
    }

    // Delete a real estate
    public void deleteRealEstate(int id) {
        realEstateRepository.deleteById(id);
    }

    // Hàm lấy thông tin tên của cột trên db thông qua attribute của object
    public String getColumnName(String fieldName) {
        try {
            Field field = Realestate.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }


    // Hàm thông qua id của xã huyện tỉnh, user để lấy ra thông tin tên và gán vào Realestate
    public void setNameForCollumn(Realestate realestate) {
                Integer provinceid = realestate.getProvinceId();
                Integer districtid = realestate.getDistrictId();
                Integer wardid = realestate.getWardsId();
                Long userId = realestate.getCustomerId();
                Integer streetid = realestate.getStreetId();
                Integer projectId = realestate.getProjectId();
                // Lấy ra các tên của tỉnh quận huyện rồi gán vào provinceName, districtName, wardName, Streetname nếu rỗng thì khỏi gán
                Province province = null;
                if(provinceid != null) {
                    province = iProvince.findById(provinceid).orElse(null);
                }   
                District district = null;
                if(districtid != null) {
                    district = iDistrict.findById(districtid).orElse(null);
                }
                Ward ward = null;
                if(wardid != null) {
                    ward = iWard.findById(wardid).orElse(null);
                } 

                Street street = null;
                if(streetid != null) {
                    street = iStreet.findById(streetid).orElse(null);
                }

                User user = null;
                if(userId != null) {
                    user = iUser.findById(userId).orElse(null);
                }

                Project project = null;
                if(projectId != null) {
                    project = iProject.findById(projectId).orElse(null);
                }

                if(province!= null) {
                    realestate.setProvinceName(province.getName() + ", " + province.getCode());
                }

                if(district != null) {
                    realestate.setDistrictName(district.getPrefix() + " " + district.getName());
                }

                if(ward != null) {
                    realestate.setWardName(ward.getPrefix() + " " + ward.getName());
                }

                if(user != null) {
                    realestate.setCustomerName(user.getFirstName() + " " + user.getLastName());
                    realestate.setEmail(user.getEmail());
                    realestate.setPhoneNumber(user.getPhoneNumber());
                }

                if(street != null) {
                    realestate.setStreetName(street.getPrefix() + " " + street.getName());
                }

                if(project != null) {
                    realestate.setProjectName(project.getName());
                }
    }

    // Hàm lấy ra danh sách cả thuê và bán mới thêm gần đây
    public List<Realestate> getRecentRealestate() {
        List<Realestate> listRealestatesRecent = realEstateRepository.getAllRecentRealestate();
        for(Realestate realestate : listRealestatesRecent) {
            setNameForCollumn(realestate);
        }
        return listRealestatesRecent;
    }

    // Hàm lấy ra danh sách bán mới thêm gần đây
    public List<Realestate> getRecentRealestateForSale() {
        List<Realestate> listRealestatesRecent = realEstateRepository.getAllRecentRealestateForSale();
        for(Realestate realestate : listRealestatesRecent) {
            setNameForCollumn(realestate);
        }
        return listRealestatesRecent;
    }


    // Hàm lấy ra danh sách thuê mới thêm gần đây
    public List<Realestate> getRecentRealestateForRent() {
        List<Realestate> listRealestatesRecent = realEstateRepository.getAllRecentRealestateForRent();
        for(Realestate realestate : listRealestatesRecent) {
            setNameForCollumn(realestate);
        }
        return listRealestatesRecent;
    }


    // Hàm thay đổi trạng thái của bất động sản
    public void changeRealestateStatus(Integer id, Integer status) {
        Realestate realestate = realEstateRepository.findById(id).orElse(null);
        realestate.setStatus(status);
        realEstateRepository.save(realestate);  
    }


    // Hàm validate 1 số thông tin về bất động sản
    public String validateRealestate(Realestate realestate) {
        String result = "ok";
        if(realestate.getPrice() == null && realestate.getRequest() == 0) {
            return "Giá bán không được bỏ trống";
        }

        if(realestate.getPriceRent() == null && realestate.getRequest() == 1) {
            return "Giá cho thuê không được bỏ trống";
        }

        if(realestate.getLongX() < realestate.getWidthY()) {
            return "Chiều dài bất động sản không được nhỏ hơn chiều rộng";
        }

        return result;
    }
}
