package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer>{
    @Query(value = "SELECT * FROM `ward` WHERE `_district_id` =:districtId", nativeQuery = true)
    List<Ward> getAllWardByDistrictId(@Param("districtId") int districtId);

    // Hàm lấy ra danh sách xã có tên giống name
    Page<Ward> findByNameContainingIgnoreCase(String name, Pageable pageable);

    // Hàm lấy ra danh sách xã theo province Id
    @Query(value = "SELECT * FROM ward " +
                   "WHERE _province_id = :provinceId AND _name LIKE %:name%", nativeQuery = true)
    Page<Ward> filterWardByProvinceIdAndName(@Param("name") String name, @Param("provinceId") int provinceId, Pageable pageable);

    // Hàm lấy ra danh sách xã theo districtId
    @Query(value = "SELECT * FROM ward " + 
                   "WHERE _district_id = :districtId AND _name LIKE %:name%", nativeQuery = true)
    Page<Ward> filterWardByDistrictIdAndName(@Param("name") String name, @Param("districtId") int districtId, Pageable pageable);
}
