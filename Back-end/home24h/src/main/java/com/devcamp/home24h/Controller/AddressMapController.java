package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.AddressMap;
import com.devcamp.home24h.Repository.AddressMapRepository;
import com.devcamp.home24h.Service.AddressMapService;

@RestController
@CrossOrigin
public class AddressMapController {
    @Autowired
    AddressMapService iAddressMapService;

    @Autowired
    AddressMapRepository iAddressMapRepository;
    
    @GetMapping("/addressmaps")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_HOME_SELLER')")
    public ResponseEntity<List<AddressMap>> findAll() {
        try {
            return new ResponseEntity<>(iAddressMapService.getAllAddressMap(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // lấy ra addressmap có phân trang
    @GetMapping("/address-maps")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_HOME_SELLER')")
    public ResponseEntity<Object> getAddressMapByPage(@RequestParam(defaultValue = "0") int page, 
                                                      @RequestParam(defaultValue = "10") int size,
                                                      @RequestParam(defaultValue = "") String keyword,
                                                      @RequestParam(defaultValue = "id") String sortBy,
                                                      @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<AddressMap> addressMapByPage = iAddressMapService.getAddressMapByPage(page, size, keyword, sortBy, sortType);
            return new ResponseEntity<>(addressMapByPage, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // Get Data addressmap By id
    @GetMapping("addressmaps/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<AddressMap> getAddressMapById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iAddressMapRepository.findById(id).get() ,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addressmaps")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<AddressMap> createAddressMap(@Valid @RequestBody AddressMap pAddressMap) {
        try {
            return new ResponseEntity<>(iAddressMapService.createAddressMap(pAddressMap), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/addressmaps/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<AddressMap> updateAddressMap(@Valid @RequestBody AddressMap pAddressMap,@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iAddressMapService.updateAddressMap(pAddressMap, id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/address-maps/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<AddressMap> deleleAddressMap(@PathVariable("id") int id) {
        try {
            iAddressMapService.deleteAddressMap(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
