package com.devcamp.home24h.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.Investor;

public interface InvestorRepository extends JpaRepository<Investor, Integer>{
    @Query(value = "SELECT * FROM investor WHERE " + 
        "name LIKE %:keyword% " + 
        "OR phone LIKE %:keyword% " + 
        "OR fax LIKE %:keyword% " + 
        "OR email LIKE %:keyword% " + 
        "OR phone2 LIKE %:keyword%", nativeQuery = true)
    Page<Investor> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
