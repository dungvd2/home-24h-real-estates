package com.devcamp.home24h.Service;

import com.devcamp.home24h.Entity.Token;

public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);
}
