package com.devcamp.home24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.home24h.Entity.Location;

public interface LocationRepository extends JpaRepository<Location, Integer>{
    
}
