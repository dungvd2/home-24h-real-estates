package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.Street;

public interface StreetRepository extends JpaRepository<Street, Integer>{
    @Query(value = "SELECT * FROM `street` WHERE `_district_id` =:districtId", nativeQuery = true)
    List<Street> getAllWardByDistrictId(@Param("districtId") int districtId);

    // Hàm lấy ra danh sách xã có tên giống name
    Page<Street> findByNameContainingIgnoreCase(String name, Pageable pageable);

    // Hàm lấy ra danh sách xã theo province Id
    @Query(value = "SELECT * FROM street " +
                   "WHERE _province_id = :provinceId AND _name LIKE %:name%", nativeQuery = true)
    Page<Street> filterStreetByProvinceIdAndName(@Param("name") String name, @Param("provinceId") int provinceId, Pageable pageable);

    // Hàm lấy ra danh sách xã theo districtId
    @Query(value = "SELECT * FROM street " + 
                   "WHERE _district_id = :districtId AND _name LIKE %:name%", nativeQuery = true)
    Page<Street> filterStreetByDistrictIdAndName(@Param("name") String name, @Param("districtId") int districtId, Pageable pageable);
}
