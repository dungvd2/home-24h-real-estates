package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.Project;

import com.devcamp.home24h.Repository.ProjectRepository;
import com.devcamp.home24h.Repository.RealestateRepository;
import com.devcamp.home24h.Service.ProjectService;

@RestController
@CrossOrigin
public class ProjectController {
    @Autowired
    private ProjectService projectService;


    @Autowired
    private ProjectRepository iProject;

    @Autowired
    RealestateRepository iRealestate;

    @GetMapping("/projects")
    public ResponseEntity<List<Project>> getAllProjects() {
        try {
            List<Project> projects = projectService.getAll();
            return new ResponseEntity<>(projects, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/projects/filter")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> filterProject(@RequestParam(defaultValue = "") String name,
                                                @RequestParam(defaultValue = "0") int provinceId,
                                                @RequestParam(defaultValue = "0") int districtId,
                                                @RequestParam(defaultValue = "0") int page,
                                                @RequestParam(defaultValue = "10") int size,
                                                @RequestParam(defaultValue = "id") String sortBy,
                                                @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<Project> projects = projectService.filterProject(name, provinceId, districtId, page, size, sortBy, sortType);
            for(Project project : projects) {
                // Gọi hàm tìm tên của xã huyện tỉnh, đơn vị thiết kế nhầ thầu, nhà đầu tư
                projectService.fillNameForColumn(project);
            }
            return new ResponseEntity<>(projects, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra thông tin project bằng Id
    @GetMapping("/projects/{id}")
    public ResponseEntity<Project> getProjectById(@PathVariable("id") int id) {
        try {
            Project project = iProject.findById(id).orElse(null);
            return new ResponseEntity<>(project, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // API lấy ra danh sách các dự án tương ứng bằng huyện
    @GetMapping("/projects/districts/{id}")
    public ResponseEntity<Object> getListProjectByDistrictId(@PathVariable("id") int id) {
        try {
            List<Project> projects = iProject.findByDistrictId(id);
            return new ResponseEntity<>(projects, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/projects")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Project> createProject(@Valid @RequestBody Project project) {
        try {
            Project createdProject = projectService.createProject(project);
            return new ResponseEntity<>(createdProject, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/projects/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Project> updateProject(@PathVariable("id") int id, @Valid @RequestBody Project project) {
        try {
            Project updatedProject = projectService.updateProject(project, id);
            return new ResponseEntity<>(updatedProject, HttpStatus.OK);
    
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/projects/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteProject(@PathVariable("id") int id) {
        try {
            Long numRealEstate = iRealestate.countByProjectId(id);
            if(numRealEstate > 0) {
                return ResponseEntity.badRequest().body("Có " + numRealEstate + " bất động sản liên quan, không thế xóa dự án!");
            }
            projectService.deleteProject(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>("Có lỗi xảy ra, vui lòng thử lại sau", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

