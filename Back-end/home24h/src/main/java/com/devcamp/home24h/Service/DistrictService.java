package com.devcamp.home24h.Service;


import java.lang.reflect.Field;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.District;
import com.devcamp.home24h.Entity.Province;
import com.devcamp.home24h.Repository.DistrictRepository;
import com.devcamp.home24h.Repository.ProvinceRepository;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository iDistrict;

    @Autowired
    ProvinceRepository iProvince;
    
    // Hàm lấy ra toàn bộ district
    public Page<District> getListDistrictByPage(int page, int size, String keyword, String sortBy, String sortType) {
        String filled = getColumnName(sortBy);
        if(filled == null) {
            filled = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filled);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iDistrict.findAllByKeyword(keyword, pageable);
        
    }

    // Hàm tạo mới 1 district
    public District createDistrict(District pDistrict, Integer provinceId) {
        Province vProvince = iProvince.findById(provinceId).get();
        pDistrict.setProvince(vProvince);
        District districtCreated = iDistrict.save(pDistrict);
        return districtCreated;
    }

    // Hàm cập nhật 1 district
    public District updateDistrict(District pDistrict, Integer provinceId, Integer districtId) {
        Province vProvince = iProvince.findById(provinceId).get();
        District vDistrict = iDistrict.findById(districtId).get();
        vDistrict.setProvince(vProvince);
        vDistrict.setName(pDistrict.getName());
        vDistrict.setPrefix(pDistrict.getPrefix());
        District updatedDistrict = iDistrict.save(vDistrict);
        return updatedDistrict;
    }

    // Hàm xóa 1 district
    public void deleteDistrict(Integer districtId){
        iDistrict.deleteById(districtId);
    }

     // Hàm lấy ra tên column
    public String getColumnName(String fieldName) {
        try {
            Field field = District.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
