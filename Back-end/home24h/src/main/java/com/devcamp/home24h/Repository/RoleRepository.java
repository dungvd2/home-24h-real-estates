package com.devcamp.home24h.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.home24h.Entity.Role;


public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRoleKey(String roleKey);
}
