package com.devcamp.home24h.Entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Vui lòng nhập tên dự án")
    @Column(name = "_name")
    private String name;

    @Positive(message = "ID của tỉnh là số dương từ 1 đến 64")
    @Column(name = "_province_id")
    private Integer provinceId;

    @Positive(message = "ID của huyện là số dương lớn hơn 0")
    @Column(name = "_district_id")
    private Integer districtId;

    @Positive(message = "ID của xã là số dương lớn hơn 0")
    @Column(name = "_ward_id")
    private Integer wardId;

    @Positive(message = "ID của đường phố là số dương lớn hơn 0")
    @Column(name = "_street_id")
    private Integer streetId;

    @Column(name = "address", length = 1000)
    private String address;

    @Column(columnDefinition = "MEDIUMTEXT")
    private String slogan;

    @Column(name = "description")
    private String description;

    @Column(name = "acreage")
    private BigDecimal acreage;

    @Positive(message = "Diện tích xây dựng phải lớn hơn 0")
    @Column(name = "construct_area")
    private BigDecimal constructArea;

    @Positive(message = "Số tòa phải lớn hơn  0")
    @Column(name = "num_block")
    private Integer numBlock;

    @Column(name = "num_floors", length = 5000)
    private String numFloors;

    @NotNull(message = "Vui lòng nhập số căn hộ")
    @Positive(message = "Số căn hộ phải lớn hơn 0")
    @Column(name = "num_apartment", nullable = false)
    private Integer numApartment;

    @Column(name = "apartmentt_area", length = 5000)
    private String apartmentArea;

    @NotNull(message = "Vui lòng nhập ID của nhà đầu tư")
    @Positive(message = "ID nhà đầu tư phải là số lớn hơn 0")
    @Column(nullable = false)
    private Integer investor;

    @Positive(message = "ID nhà thầu phải là số lớn hơn 0")
    @Column(name = "construction_contractor")
    private Integer constructionContractor;

    @Positive(message = "ID đơn vị thiết kế phải là số lớn hơn 0")
    @Column(name = "design_unit")
    private Integer designUnit;

    @NotEmpty(message = "Vui lòng chọn ít nhất 1 tiện ích")
    @Column(length = 10000, nullable = false)
    private String utilities;

    @NotEmpty(message = "Vui lòng chọn ít nhất 1 kết nối vùng")
    @Column(name = "region_link", length = 1000)
    private String regionLink;

    @Column(length = 5000)
    private String photo;

    @Column(name = "_lat")
    private Double latitude;

    @Column(name = "_lng")
    private Double longitude;

    @Transient
    private String provinceName;

    @Transient
    private String districtName;

    @Transient
    private String investorName;

    @Transient
    private String contractorName;

    @Transient 
    private String designUnitName;

    public Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Integer numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Integer getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(Integer numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public Integer getInvestor() {
        return investor;
    }

    public void setInvestor(Integer investor) {
        this.investor = investor;
    }

    public Integer getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(Integer constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public Integer getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(Integer designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getDesignUnitName() {
        return designUnitName;
    }

    public void setDesignUnitName(String designUnitName) {
        this.designUnitName = designUnitName;
    }

    
}
