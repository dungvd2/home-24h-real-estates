package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.RegionLink;

public interface RegionLinkRepository extends JpaRepository<RegionLink, Integer>{
    @Query(value = "SELECT * FROM `region_link`", nativeQuery = true)
    List<RegionLink> getAllRegionLinks();

    @Query(value = "SELECT * FROM region_link " + 
        "WHERE name LIKE %:keyword% " +
        "OR _lat LIKE %:keyword% " + 
        "OR _lng LIKE %:keyword% ", nativeQuery = true)
    Page<RegionLink> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
