package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.District;

public interface DistrictRepository extends JpaRepository<District, Integer>{
    @Query(value = "SELECT * FROM `district` WHERE `_province_id` =:provinceId", nativeQuery = true)
    List<District> getListDistrictsByProvinceId(@Param("provinceId") int provinceId);

    // Lấy ra danh sách có phân trang theo tên và tỉnh
    @Query(value = "SELECT * FROM `district`WHERE `_name` LIKE %:name% AND `_province_id` = :provinceId", nativeQuery = true)
    Page<District> findByPartialNameAndProvinceId(@Param("name") String name, @Param("provinceId") int provinceId, Pageable pageable);
    
    // Lấy ra danh sách có phân trang theo tên
    Page<District> findByNameContainingIgnoreCase(String name, Pageable pageable);

    @Query(value = "SELECT * FROM district d " +
        "LEFT JOIN province p ON d._province_id = p.id " + 
        "WHERE p._name LIKE %:keyword% " +
        "OR p._code LIKE %:keyword% " +
        "OR d._name LIKE %:keyword% " +
        "OR d._prefix LIKE %:keyword% ", nativeQuery = true)
    Page<District> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
