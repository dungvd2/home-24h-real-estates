package com.devcamp.home24h.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.ConstructionContractor;
import com.devcamp.home24h.Entity.DesignUnit;
import com.devcamp.home24h.Entity.District;
import com.devcamp.home24h.Entity.Investor;
import com.devcamp.home24h.Entity.Project;
import com.devcamp.home24h.Entity.Province;
import com.devcamp.home24h.Repository.ConstructionContractorRepository;
import com.devcamp.home24h.Repository.DesignUnitRepository;
import com.devcamp.home24h.Repository.DistrictRepository;
import com.devcamp.home24h.Repository.InvestorRepository;
import com.devcamp.home24h.Repository.ProjectRepository;
import com.devcamp.home24h.Repository.ProvinceRepository;

@Service
public class ProjectService {
    

    @Autowired
    ProjectRepository projectRepository;
    
    @Autowired
    DistrictRepository iDistrict;

    @Autowired
    ProvinceRepository iProvince;

    @Autowired 
    DesignUnitRepository iDesignUnit;

    @Autowired
    ConstructionContractorRepository iContractor;

    @Autowired
    InvestorRepository iInvestor;

    // Get all projects
    public List<Project> getAll() {
        return projectRepository.findAll();
    }   

    // Lấy ra danh sách của project có phân trang và lọc
    public Page<Project> filterProject(String name, int provinceId, int districtId, int page, int size, String sortBy, String sortType) {
        
        String filed = this.getColumnName(sortBy);
        if(filed == null) {
            filed = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filed);
        Pageable pageable = PageRequest.of(page, size, sort);
        if(provinceId == 0)  {

            return projectRepository.findByNameContainingIgnoreCase(name, pageable);
        }
        else if(districtId == 0) {
            return projectRepository.findByNameAndProvinceId(name, provinceId, pageable);
        }
        else {
            return projectRepository.findByNameAndDistrictId(name, districtId, pageable);
        }
    }

    // Create a new project
    public Project createProject(Project project) {
        return projectRepository.save(project);
    }

    // Update a project
    public Project updateProject(Project project, int id) {
        Project existingProject = projectRepository.findById(id).get();
        existingProject.setName(project.getName());
        existingProject.setProvinceId(project.getProvinceId());
        existingProject.setDistrictId(project.getDistrictId());
        existingProject.setWardId(project.getWardId());
        existingProject.setStreetId(project.getStreetId());
        existingProject.setAddress(project.getAddress());
        existingProject.setSlogan(project.getSlogan());
        existingProject.setDescription(project.getDescription());
        existingProject.setAcreage(project.getAcreage());
        existingProject.setConstructArea(project.getConstructArea());
        existingProject.setNumBlock(project.getNumBlock());
        existingProject.setNumFloors(project.getNumFloors());
        existingProject.setNumApartment(project.getNumApartment());
        existingProject.setApartmentArea(project.getApartmentArea());
        existingProject.setInvestor(project.getInvestor());
        existingProject.setConstructionContractor(project.getConstructionContractor());
        existingProject.setDesignUnit(project.getDesignUnit());
        existingProject.setUtilities(project.getUtilities());
        existingProject.setRegionLink(project.getRegionLink());
        existingProject.setPhoto(project.getPhoto());
        existingProject.setLatitude(project.getLatitude());
        existingProject.setLongitude(project.getLongitude());
        return projectRepository.save(existingProject);
    }

    // Delete a project
    public void deleteProject(int id) {
        projectRepository.deleteById(id);
    }

    public String getColumnName(String fieldName) {
        try {
            Field field = Project.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Hàm thiết lập tên xã huyện tỉnh, nhà thầu đơn vị thiết kê, nhà đầu tư cho project theo id
    public Project fillNameForColumn(Project project) {
        Integer provinceid = project.getProvinceId();
                Integer districtid = project.getDistrictId();
                Integer investorId = project.getInvestor();
                Integer constractorId = project.getConstructionContractor();
                Integer designUnitId = project.getDesignUnit();

                Province province = null;
                if(provinceid != null) {
                    province = iProvince.findById(provinceid).orElse(null);
                }   

                District district = null;
                if(districtid != null) {
                    district = iDistrict.findById(districtid).orElse(null);
                }

                DesignUnit designUnit = null;
                if(designUnitId != null) {
                    designUnit = iDesignUnit.findById(designUnitId).orElse(null);
                }

                ConstructionContractor contractor = null;
                if(constractorId != null) {
                    contractor = iContractor.findById(constractorId).orElse(null);
                }

                Investor investor = null;
                if(investorId != null) {
                    investor = iInvestor.findById(investorId).orElse(null);
                }

                if(province!= null) {
                    project.setProvinceName(province.getName() + ", " + province.getCode());
                }

                if(district != null) {
                    project.setDistrictName(district.getPrefix() + " " + district.getName());
                }

                if(designUnit != null) {
                    project.setDesignUnitName(designUnit.getName());
                }

                if(contractor != null) {
                    project.setContractorName(contractor.getName());
                }

                if(investor != null) {
                    project.setInvestorName(investor.getName());
                }
                return project;
    }

    // Hàm xử lý trường hợp xóa 1 region link trong project
    // Nếu chỉ có 1 tiện ích thì đặt bằng null luôn, nếu nhiều hơn thì tách ra thành mảng bằng hàm split rồi tìm phần tử trùng
    public void updateProjectWhenDeleteRegionLink(Integer regionLinkId) {
        List<Project> projects = projectRepository.getAllProjectHasRegionLink(regionLinkId);
        String regionLinkIdString = "" + regionLinkId;

        for(Project project : projects){
            String regionLink = project.getRegionLink();
            if(regionLink.length() == 1) {
                project.setRegionLink(null);
            }
            else {
                String[] arrRegionLink = regionLink.split(",");
                List<String> listRegionLink = new ArrayList<String>(Arrays.asList(arrRegionLink)); // Biến nó thành list;
                int position = listRegionLink.indexOf(regionLinkIdString); // Tìm vị trí bằng hàm indexof
                listRegionLink.remove(position); // Xóa ra khỏi list bằng hàm remove
                String newRegionLink = String.join(",", listRegionLink); // Gộp lại bằng hàm join
                project.setRegionLink(newRegionLink);
            }
            // Lưu lại
            projectRepository.save(project);
        }
    }

    // Hàm xử lý trường hợp xóa 1 tiện ích trong project
    // Nếu chỉ có 1 tiện ích thì đặt bằng null luôn, nếu nhiều hơn thì tách ra bằng split rồi tìm phần tử trùng
    public void updateProjectWhenDeleteUtilites(Integer utilitiesId) {
        List<Project> projects = projectRepository.getAllProjectHasUtilities(utilitiesId);
        String deleteUtilities = "" + utilitiesId;
        for(Project project: projects) {
            String strUtilities = project.getUtilities();
            if(strUtilities.length() == 1) {
                project.setUtilities(null);
            }
            else {
                String[] arrUtilities = strUtilities.split(","); // CHuyển thành chuỗi thông qua hàm split
                List<String> listUtilities = new ArrayList<String>(Arrays.asList(arrUtilities)); // Biến nó thành list;
                int position = listUtilities.indexOf(deleteUtilities); // Tìm vị trí bằng hàm indexof
                listUtilities.remove(position); // Xóa ra khỏi list bằng hàm remove
                String newUtilities = String.join(",", listUtilities); // Gộp lại bằng hàm join
                project.setUtilities(newUtilities);
            }
            projectRepository.save(project);
        }
    }


}
