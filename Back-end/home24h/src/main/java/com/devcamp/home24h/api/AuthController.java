package com.devcamp.home24h.api;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.Role;
import com.devcamp.home24h.Entity.Token;
import com.devcamp.home24h.Entity.User;
import com.devcamp.home24h.Repository.UserRepository;
import com.devcamp.home24h.security.JwtUtil;
import com.devcamp.home24h.security.UserPrincipal;
import com.devcamp.home24h.Service.AccountService;
import com.devcamp.home24h.Service.EmailService;
import com.devcamp.home24h.Service.PasswordResetService;
import com.devcamp.home24h.Service.SmsService;
import com.devcamp.home24h.Service.TokenService;
import com.devcamp.home24h.Service.UserService;
import com.devcamp.home24h.Service.ValidateService;

@RestController
public class AuthController {

    @Autowired
    private UserRepository iUser;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordResetService passwordResetService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private ValidateService validateService;

    // API đăng ký gán role mặc định là customer và tạo mới user
    @CrossOrigin
    @PostMapping("/users/register")
    public ResponseEntity<Object> register(@RequestBody User user) {
        return accountService.checkAndRegisterUser(user);
    }

    // Hàm login, kiểm tra tài khoản mật khẩu có khớp không sau đó là tạo ra token
    @CrossOrigin
    @PostMapping("/users/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
        if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Tài khoản hoặc mật khẩu không chính xác");
        }
        User userCheck = iUser.findByUsername(userPrincipal.getUsername());
        if(userCheck.isDeleted()) {
            return ResponseEntity.badRequest().body("Tài khoản đã bị vô hiệu hóa!");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        // String randomcode = RandomStringUtils.random(6, false, true); //RandomStringUtils.random( Số kí tự, chứa chữ, chứa số);
        // smsService.sendSms("+84917354990", randomcode);
        // emailService.sendEmail("duydung1271998@gmail.com", "Cảnh báo đăng nhập!", "Tài khoản " + user.getUsername() + " vừa đăng nhập vào lúc " + randomcode + " !");
        
        return ResponseEntity.ok(token.getToken());

    }

    
    @GetMapping("/hello")
	/*
	 * @PreAuthorize("hasRole('USER_READ') " + "|| hasRole('USER_CREATE')" +
	 * "|| hasRole('USER_UPDATE')" + "|| (hasRole('USER_DELETE')")
	 */
    public ResponseEntity<Object> hello() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
        // Set<String> roles = authentication.getAuthorities().stream()
        // 	     .map(r -> r.getAuthority()).collect(Collectors.toSet());
        // boolean hasUserRole = authentication.getAuthorities().stream()
        //         .anyMatch(r -> r.getAuthority().equals("ROLE_USER"));
        return ResponseEntity.ok(userPrincipal);
    }


    // Hàm để kiểm tra quyền của từng user sau đó navigation về từng trang hợp lý
    @CrossOrigin
    @GetMapping("/users/hello1")
    public ResponseEntity<Object> hello1() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = ((UserPrincipal) authentication.getPrincipal());
            return ResponseEntity.ok(userPrincipal);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }
    
    @GetMapping("/hello3")
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public ResponseEntity<Object> hello3() {
        return ResponseEntity.ok("hello cho ADMIN va CUSTOMER");
    }
    
    @GetMapping("/hello4")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Object> hello4() {
        return ResponseEntity.ok("hello chi cho ADMIN");
    }

    // Hàm tạo ra pass code mới để thực hiện yêu cầu 
    // Lấy ra user từ token, tạo ra passcode từ hàm random sau đó gán cho user và gán thời gian hết hạn đồng thời gửi mail pass code
    @CrossOrigin
    @GetMapping("/users/create-passcode")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createPassCode(@RequestParam(defaultValue = "Home 24h Request") String requestContext) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            User user = iUser.findByUsername(userPrincipal.getUsername());
            String randomPasscode = RandomStringUtils.random(6, false, true);
            // Service gửi mail
            emailService.sendEmail("duydung1271998@gmail.com", requestContext, "Mã bảo vệ là: " + randomPasscode);
            user.setPassCode(randomPasscode);
            user.setPassCodeExpiredTime(passwordResetService.getPassCodeExpiredTime());
            iUser.save(user);
            return ResponseEntity.ok("Created Success!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
        
    }


    // API tạo ra passcode và gửi về email nhằm lấy lại mật khẩu
    @CrossOrigin
    @GetMapping("/users/forgot/email")
    public ResponseEntity<Object> createPassCodeAndSendEmailToGetNewPassword(@RequestParam(defaultValue = "") String username) {
        try {
            if(username.equals("")) {
                return ResponseEntity.badRequest().body("Bạn chưa nhập tên tài khoản");
            }
            User user = iUser.findByUsername(username);
            String randomPasscode = RandomStringUtils.random(6, false, true);
            emailService.sendEmail(user.getEmail(), "Yêu cầu cấp mật khẩu mới", randomPasscode);
            user.setPassCode(randomPasscode);
            user.setPassCodeExpiredTime(passwordResetService.getPassCodeExpiredTime());
            iUser.save(user);
            String email = user.getEmail();
            // Mã hóa email để trả về nhắc nhở người dùng
            String emailEncode = email.substring(0,2) + "***" + email.substring(email.length() - 5);
            return ResponseEntity.ok(emailEncode);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API tạo ra passcode và gửi về số điện thoại nhằm lấy lại mật khẩu
    @CrossOrigin
    @GetMapping("/users/forgot/phone")
    public ResponseEntity<Object> createPassCodeAndSendPhoneNumberToGetNewPassword(@RequestParam(defaultValue = "") String username) {
        try {
            if(username.equals("")) {
                return ResponseEntity.badRequest().body("Bạn chưa nhập tên tài khoản");
            }
            User user = iUser.findByUsername(username);
            String randomPasscode = RandomStringUtils.random(6, false, true);
            String phone = user.getPhoneNumber();
            String phoneNumberWithRegionCode = "+84" + phone.substring(1);
            smsService.sendSms(phoneNumberWithRegionCode, randomPasscode);
            user.setPassCode(randomPasscode);
            user.setPassCodeExpiredTime(passwordResetService.getPassCodeExpiredTime());
            iUser.save(user);
            String phoneEncode = phone.substring(0, 2) + "******" + phone.substring(phone.length()-3);
            return ResponseEntity.ok(phoneEncode);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API thay đổi role của user
    // Input: Username, rokey, passcode
    // Output: Thông qua token tìm ra useradmin, sau đó đối chiếu với cái mã bảo vệ đã tạo và thời hạn nếu hợp lệ thì cho thay đổi role user và lưu lại
    @CrossOrigin
    @PutMapping("/users/change-role")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> changeRoleUsers(@RequestParam String username, @RequestParam String roleKey, @RequestParam String passCode) {
        try {
            ResponseEntity<Object> changeRoleResult = accountService.changeRoleUser(username, roleKey, passCode);
            if(changeRoleResult != null) {
                return changeRoleResult;
            }
            return ResponseEntity.ok("Thay đổi role thành công!");
            
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API tạo tài khoản của admin
    // Nếu khác role customer thì bắt buộc phải nhập mã bảo vệ kiểm tra chính xác mới cho tạo
    @CrossOrigin
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/users/admin-create")
    public ResponseEntity<Object> createUserByAdmin(@Valid @RequestBody User user, 
                                                    @RequestParam(defaultValue = "ROLE_CUSTOMER") String roleKey, 
                                                    @RequestParam(defaultValue = "") String passCode) {
        try {
            // Kiểm tra xem trùng username, emaill, sđt hay không
            String vMessageValidate = validateService.validateEmailAndPhoneNumberForUser(user.getEmail(), user.getPhoneNumber());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(vMessageValidate);
            }
            ResponseEntity<Object> duplicateCheckResult =  accountService.checkForDuplicates(user);
            if(duplicateCheckResult != null) {
                return duplicateCheckResult;
            }

            // Kiểm tra pass code và tạo
            ResponseEntity<Object> accountCreatedCheck = accountService.checkAndCreateUser(user, roleKey, passCode);
            if(accountCreatedCheck != null) {
                return accountCreatedCheck;
            } 
            return ResponseEntity.ok("Tạo mới tài khoản thành công!");
 
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API sửa thông tin user
    // Input: Đối tượng user cần sửa, 
    @CrossOrigin
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/users/admin-update")
    public ResponseEntity<Object> updateUserByAdmin(@Valid @RequestBody User pUser, 
                                                    @RequestParam(defaultValue = "") String username,
                                                    @RequestParam(defaultValue = "ROLE_CUSTOMER") String roleKey, 
                                                    @RequestParam(defaultValue = "") String passCode) {
        try {
             // Kiểm tra xem trùng emaill, sđt hay không
            String vMessageValidate = validateService.validateEmailAndPhoneNumberForUser(pUser.getEmail(), pUser.getPhoneNumber());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(vMessageValidate);
            }
            ResponseEntity<Object> duplicateCheckResult =  accountService.checkForDuplicatesEmailOrPhone(pUser);
            if(duplicateCheckResult != null) {
                return duplicateCheckResult;
            }
            // Kiểm tra pass code và tạo
            ResponseEntity<Object> accountUpdatedCheck = accountService.checkAndUpdateUser(username, pUser, roleKey, passCode);
            if(accountUpdatedCheck != null) {
                return accountUpdatedCheck;
            } 
            return ResponseEntity.ok("Cập nhật tài khoản thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }


    // API xóa tài khoản của một user
    // Thiết lập isdeleted thành true
    // Nếu tài khoản có role khác role khách hàng thì phải thông qua xác nhận mã bảo vệ
    @CrossOrigin
    @PutMapping("/users/delete")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteUser(@RequestParam(defaultValue = "") String username, @RequestParam(defaultValue = "") String passCode) {
        try {
            User user = iUser.findByUsername(username);
            user.setDeleted(true);
            List<Role> roles = new ArrayList<>(user.getRoles());
            String currentRoleKey = roles.get(0).getRoleKey();
            // Kiểm tra role hiện tại có giống customer ko cần xét mã
            if(currentRoleKey.equals("ROLE_CUSTOMER")) {
                iUser.save(user);
                return ResponseEntity.ok("Tài khoản đã bị vô hiệu hóa!");
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            User userAdmin = iUser.findByUsername(userPrincipal.getUsername());
            Boolean isPassCodeValid = passwordResetService.isPassCodeValid(userAdmin, passCode);
            if(!isPassCodeValid) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Mã bảo vệ không hợp lệ!");
            }
            iUser.save(user);
            return ResponseEntity.ok("Tài khoản đã bị vô hiệu hóa!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API xóa tài khoản của một user
    // Thiết lập isdeleted từ true thành false

    @CrossOrigin
    @PutMapping("/users/restore")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> restoreUser(@RequestParam(defaultValue = "") String username) {
        try {
            User user = iUser.findByUsername(username);
            user.setDeleted(false);
            iUser.save(user);
            return ResponseEntity.ok("Tài khoản đã được khôi phục!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }


    // API lấy ra thông tin user thông qua username
    @CrossOrigin
    @GetMapping("/users/")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getUserById(@RequestParam(defaultValue = "") String username) {
        try {
            User user = iUser.findByUsername(username);
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API kiểm tra passcode có chính xác hay không
    // Nếu đúng thì set cho hết hạn luôn
    // Kết quả trả về là true hoặc false
    @CrossOrigin
    @GetMapping("/users/check")
    public ResponseEntity<Object> checkIsPasscodeValid(@RequestParam(defaultValue = "") String username, @RequestParam(defaultValue = "") String passCode) {
        try {
            User user = iUser.findByUsername(username);
            if(user == null) {
                return ResponseEntity.badRequest().body("username không tồn tại");
            }
            Boolean isPasscodeValid = passwordResetService.isPassCodeValid(user, passCode);
            return ResponseEntity.ok(isPasscodeValid);

        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }


    // API tạo mới mật khẩu trong phần quên mật khẩu
    @CrossOrigin
    @PutMapping("/users/forget/create-password")
    public ResponseEntity<Object> createNewPassword(@RequestBody User pUser) {
        try {
            User user = iUser.findByUsername(pUser.getUsername());
            if(user == null) {
                return ResponseEntity.badRequest().body("Người dùng không tồn tại");
            }
            Boolean isPasscodeValid = passwordResetService.isPassCodeValid(user, pUser.getPassCode());
            if(!isPasscodeValid) {
                return ResponseEntity.badRequest().body("Mã bảo vệ không còn hợp lệ");
            } 
            user.setPassword(new BCryptPasswordEncoder().encode(pUser.getPassword()));
            LocalDateTime localDateTime = LocalDateTime.now();
            user.setPassCodeExpiredTime(localDateTime);
            iUser.save(user);
            return ResponseEntity.ok("Tạo mới mật khẩu thành công!");

        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API lấy ra thông tin khách hàng thông qua token
    // Trả ra 1 đối tượng user
    @CrossOrigin
    @GetMapping("/users/view")
    public ResponseEntity<Object> getUserData() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            User user = iUser.findByUsername(userPrincipal.getUsername());
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API cập nhật thông tin user
    @CrossOrigin
    @PutMapping("/users/update")
    public ResponseEntity<Object> updateUser(@RequestBody User pUser) {
        try {
            String vMessageValidate = validateService.validateEmailAndPhoneNumberForUser(pUser.getEmail(), pUser.getPhoneNumber());
            if(!vMessageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(vMessageValidate);
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            if(!new BCryptPasswordEncoder().matches(pUser.getPassword(), userPrincipal.getPassword())) {
                return ResponseEntity.badRequest().body("Mật khẩu không chính xác, vui lòng thử lại sau!");
            }
            User user = iUser.findByUsername(userPrincipal.getUsername());
            return accountService.checkAndUpdateUserByUser(user, pUser);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }

    // API cho user thay đổi mật khẩu
    @CrossOrigin
    @PutMapping("/users/change-password")
    public ResponseEntity<Object> changePassword(@RequestBody User pUser, @RequestParam String present) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            if(!new BCryptPasswordEncoder().matches(present, userPrincipal.getPassword())) {
                return ResponseEntity.badRequest().body("Mật khẩu hiện tại sai!");
            }
            if(!accountService.validatePassword(pUser.getPassword())) {
                return ResponseEntity.badRequest().body("Mật khẩu mới phải có ít nhất 6 kí tự bao gồm cả chữ lẫn số!");
            }
            User user = iUser.findByUsername(userPrincipal.getUsername());
            user.setPassword(new BCryptPasswordEncoder().encode(pUser.getPassword()));
            iUser.save(user);
            return ResponseEntity.ok("Mật khẩu đã được thay đổi");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }


    // API kiểm tra xem user có tồn tại hay không thông qua username
    @CrossOrigin
    @GetMapping("/users/find")
    public ResponseEntity<Object> isUsernameValid(@RequestParam String username) {
        try {
            User user = iUser.findByUsername(username);
            if(user == null) {
                return ResponseEntity.badRequest().body("Tên người dùng không tồn tại!");
            }

            return ResponseEntity.ok(true);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra vui lòng thử lại sau");
        }
    }
}
