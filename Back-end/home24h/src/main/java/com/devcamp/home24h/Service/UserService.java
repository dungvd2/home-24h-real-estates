package com.devcamp.home24h.Service;

import com.devcamp.home24h.Entity.User;
import com.devcamp.home24h.security.UserPrincipal;

public interface UserService {
    User createUser(User user);

    UserPrincipal findByUsername(String username);
}
