package com.devcamp.home24h.Controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.Province;
import com.devcamp.home24h.Repository.ProvinceRepository;
import com.devcamp.home24h.Service.ProvinceService;

@RestController
@CrossOrigin
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;

    @Autowired
    ProvinceRepository iProvince;

    @GetMapping("/provinces")
    public ResponseEntity<Object> getProvinceByPageAndSize(@RequestParam(defaultValue = "0") int page, 
                                                           @RequestParam(defaultValue = "10") int size,
                                                           @RequestParam(defaultValue = "") String keyword,
                                                           @RequestParam(defaultValue = "id") String sortBy,
                                                           @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<Province> provinces = provinceService.getListProvinceByPageAndSize(page, size, keyword, sortBy, sortType);
            return new ResponseEntity<>(provinces, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/list")
    public ResponseEntity<List<Province>> getAllProvince() {
        try {
            return new ResponseEntity<>(iProvince.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{id}")
    public ResponseEntity<Province> findProvinceById(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<>(iProvince.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/provinces")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Province> createProvince(@Valid @RequestBody Province province) {
        try {
            Province createdProvince = provinceService.createProvince(province);
            return new ResponseEntity<>(createdProvince, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/provinces/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Province> updateProvince(@Valid @RequestBody Province province, @PathVariable("id") int id) {
        try {
            Province updatedProvince = provinceService.updateProvince(province, id);
            return new ResponseEntity<>(updatedProvince, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/provinces/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Province> deleteProvince(@PathVariable("id") int id) {
        try {
            provinceService.deleteProvince(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

