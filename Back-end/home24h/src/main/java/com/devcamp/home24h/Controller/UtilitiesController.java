package com.devcamp.home24h.Controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.Utilities;
import com.devcamp.home24h.Repository.UtilitiesRepository;
import com.devcamp.home24h.Service.UtilitesService;

@RestController
@CrossOrigin
public class UtilitiesController {
    @Autowired
    private UtilitesService utilitiesService;

    @Autowired
    UtilitiesRepository iUtilities;

    @GetMapping("/utilities")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAllUtilitiesByPageAndSize(@RequestParam(defaultValue = "0") int page, 
                                                               @RequestParam(defaultValue = "10") int size,
                                                               @RequestParam(defaultValue = "") String keyword,
                                                               @RequestParam(defaultValue = "id") String sortBy,
                                                               @RequestParam(defaultValue = "asc") String sortType) {
        try {
            Page<Utilities> utilities = utilitiesService.getAllByPage(page, size, keyword, sortBy, sortType);
            return new ResponseEntity<>(utilities, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/utilities/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAll() {
        try {
            List<Utilities> utilities = iUtilities.findAll();
            return new ResponseEntity<>(utilities, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get utilities by Id
    @GetMapping("/utilities/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Utilities> getUtilitiesById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iUtilities.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/utilities")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Utilities> createUtilities(@Valid @RequestBody Utilities utilities) {
        try {
            Utilities createdUtilities = utilitiesService.createUtilities(utilities);
            return new ResponseEntity<>(createdUtilities, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/utilities/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Utilities> updateUtilities(@Valid @PathVariable("id") int id, @RequestBody Utilities utilities) {
        try {
            Utilities updatedUtilities = utilitiesService.updateUtilites(utilities, id);
            if (updatedUtilities != null) {
                return new ResponseEntity<>(updatedUtilities, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/utilities/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<HttpStatus> deleteUtilities(@PathVariable("id") int id) {
        try {
            utilitiesService.deleteUtilities(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
