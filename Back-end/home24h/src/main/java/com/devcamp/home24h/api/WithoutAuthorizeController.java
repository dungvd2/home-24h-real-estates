package com.devcamp.home24h.api;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.User;
import com.devcamp.home24h.Repository.UserRepository;
import com.devcamp.home24h.Service.AccountService;
import com.devcamp.home24h.Service.UserService;

@RestController
public class WithoutAuthorizeController {
	@Autowired
	private UserRepository userRepository;

    @Autowired
    private AccountService accountService;

    // Hàm gọi API để lấy ra tất cả userPricipal
    @CrossOrigin
    @GetMapping("/users/getall")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> getAllUser() {
        try {
            List<User> listUsers = userRepository.findAll();
            String result = "";
            for(User user: listUsers) {
                String userFullname = user.getFirstName() + " " + user.getLastName() + ",";
                result += user.getId() + ":" + userFullname;
            }
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm lấy ra thông tin user có phân trang
    @CrossOrigin
    @GetMapping("/users/filter")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<Object> filterUser(@RequestParam(defaultValue = "") String username,
                                             @RequestParam(defaultValue = "") String name,
                                             @RequestParam(defaultValue = "") String roleKey,
                                             @RequestParam(defaultValue = "2") Integer deleted,
                                             @RequestParam(defaultValue = "id") String sortBy,
                                             @RequestParam(defaultValue = "asc") String sortType,
                                             @RequestParam(defaultValue = "") String keyword,
                                             @RequestParam(defaultValue = "0") int page,
                                             @RequestParam(defaultValue = "10") int size) {
        try {
            Page<User> usersFilter = accountService.filterUser(username, name, roleKey, deleted, keyword ,sortBy, sortType, page, size);
            return ResponseEntity.ok(usersFilter);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Test trường hợp khôngcheck quyền Authorize
     * Tạo mới user
     * @param user
     * @return
     */
    @PostMapping("/users/createAdmin")
    public ResponseEntity<Object> create(@RequestBody User user) {
    	user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
    	return new ResponseEntity<>(userRepository.save(user),HttpStatus.CREATED);
    }
    @Autowired
    private UserService userService;
    /**
     * Test có kiểm tra quyền.
     * @param user
     * @return
     */
    @PostMapping("/users/create")
    @PreAuthorize("hasAnyAuthority('USER_CREATE')")
    public User register(@RequestBody User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return userService.createUser(user);
    }

    
}
