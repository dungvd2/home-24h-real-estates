package com.devcamp.home24h.Service;

import java.lang.reflect.Field;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.Province;
import com.devcamp.home24h.Repository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository iProvince;
    
    // Hàm lấy ra toàn bộ province
    public Page<Province> getListProvinceByPageAndSize(int page, int size, String keyword, String sortBy, String sortType) {
        String filled = getColumnName(sortBy);
        if(filled == null) {
            filled = "id";
        }
        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filled);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iProvince.findAllByKeyword(keyword, pageable);
    }

    // Hàm tạo mới 1 province
    public Province createProvince(Province pProvince) {
        Province provinceCreated = iProvince.save(pProvince);
        return provinceCreated;
    }

    // Hàm cập nhật 1 province
    public Province updateProvince(Province pProvince, Integer provinceId) {
        Province vProvince = iProvince.findById(provinceId).get();
        vProvince.setName(pProvince.getName());
        vProvince.setCode(pProvince.getCode());
        Province updatedProvince = iProvince.save(vProvince);
        return updatedProvince;
    }

    // Hàm xóa 1 province
    public void deleteProvince(Integer provinceId){
        iProvince.deleteById(provinceId);
    }

    // Hàm lấy ra tên column
    public String getColumnName(String fieldName) {
        try {
            Field field = Province.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
