package com.devcamp.home24h.Entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "t_user")
public class User extends BaseEntity {

	@NotEmpty(message = "Vui lòng nhập tên tài khoản")
	@Column(name = "username", unique = true)
	private String username;

	@NotEmpty(message = "Vui lòng nhập mật khẩu")
	@Column(name = "password")
	private String password;

	@NotEmpty(message = "Vui lòng nhập số điện thoại")
	@Column(name = "phone_number", unique = true)
	private String phoneNumber;

	@NotEmpty(message = "Vui lòng nhập email")
	@Column(name = "email_address")
	private String email;

	@NotEmpty(message = "Vui lòng nhập họ người dùng")
	@Column(name = "first_name")
	private String firstName;

	@NotEmpty(message = "Vui lòng nhập tên người dùng")
	@Column(name = "last_name")
	private String lastName;

	@Column(name = "pass_code")
	private String passCode;

	@Column(name = "pass_code_expired_time")
	private LocalDateTime passCodeExpiredTime;

	@ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinTable(name = "t_user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private Set<Role> roles = new HashSet<>();


	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the roles
	 */
	public Set<Role> getRoles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassCode() {
		return passCode;
	}

	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}

	public LocalDateTime getPassCodeExpiredTime() {
		return passCodeExpiredTime;
	}

	public void setPassCodeExpiredTime(LocalDateTime passCodeExpiredTime) {
		this.passCodeExpiredTime = passCodeExpiredTime;
	}


}
