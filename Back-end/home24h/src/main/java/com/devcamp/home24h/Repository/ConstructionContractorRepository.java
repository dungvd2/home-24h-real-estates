package com.devcamp.home24h.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.ConstructionContractor;

public interface ConstructionContractorRepository extends JpaRepository<ConstructionContractor, Integer>{
    @Query(value = "SELECT * FROM construction_contractor WHERE " + 
        "name LIKE %:keyword% " + 
        "OR phone LIKE %:keyword% " + 
        "OR fax LIKE %:keyword% " + 
        "OR email LIKE %:keyword% " + 
        "OR phone2 LIKE %:keyword%", nativeQuery = true)
    Page<ConstructionContractor> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
