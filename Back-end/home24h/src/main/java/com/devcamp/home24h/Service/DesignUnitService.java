package com.devcamp.home24h.Service;

import java.lang.reflect.Field;

import javax.persistence.Column;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.devcamp.home24h.Entity.DesignUnit;
import com.devcamp.home24h.Repository.DesignUnitRepository;
import com.devcamp.home24h.Repository.ProjectRepository;

@Service
public class DesignUnitService {
    @Autowired
    DesignUnitRepository iDesignUnit;

    @Autowired
    ProjectRepository iProject;

    // Lấy ra toàn bộ đơn vị thiết kế có phân trang
    public Page<DesignUnit> getAllDesignUnitByPage(String keyword, String sortBy, String sortType, int page, int size) {
        String filed = getColumnName(sortBy);
        if(filed == null) {
            filed = "id";
        }

        Sort sort = JpaSort.unsafe(Sort.Direction.fromString(sortType), filed);
        Pageable pageable = PageRequest.of(page, size, sort);
        return iDesignUnit.findAllByKeyword(keyword, pageable);
    }

    // Hàm tạo mới 1 đơn vị thiết kế
    public DesignUnit createDesignUnit(DesignUnit pDesignUnit) {
        DesignUnit createDesignUnit = iDesignUnit.save(pDesignUnit);
        return createDesignUnit;
    }

    // Hàm update 1 design unit
    public DesignUnit updateDesignUnit(DesignUnit pDesignUnit, Integer id) {
        DesignUnit vDesignUnit = iDesignUnit.findById(id).get();
        vDesignUnit.setAddress(pDesignUnit.getAddress()); 
        vDesignUnit.setDescription(pDesignUnit.getDescription()); 
        vDesignUnit.setEmail(pDesignUnit.getEmail()); 
        vDesignUnit.setName(pDesignUnit.getName()); 
        vDesignUnit.setPhone(pDesignUnit.getPhone()); 
        vDesignUnit.setPhone2(pDesignUnit.getPhone2()); 
        vDesignUnit.setNote(pDesignUnit.getNote()); 
        vDesignUnit.setProjects(pDesignUnit.getProjects()); 
        vDesignUnit.setWebsite(pDesignUnit.getWebsite()); 
        vDesignUnit.setFax(pDesignUnit.getFax()); 
        DesignUnit updateDesignUnit = iDesignUnit.save(vDesignUnit);
        return updateDesignUnit;
    }

    // Hàm xóa 1 design unit
    public void deleteDesignUnit(Integer id) {
        iProject.updateDesignUnitToNull(id);
        iDesignUnit.deleteById(id);
    }

    // Hàm lấy ra tên column của design unit
    public String getColumnName(String fieldName) {
        try {
            Field field = DesignUnit.class.getDeclaredField(fieldName);
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation != null && columnAnnotation.name() != null && !columnAnnotation.name().isEmpty()) {
                return columnAnnotation.name();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
