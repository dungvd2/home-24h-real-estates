package com.devcamp.home24h.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.MasterLayout;

public interface MasterLayoutRepository extends JpaRepository<MasterLayout, Integer>{
    @Query(value = "SELECT * FROM master_layout ml " + 
        "LEFT JOIN project p ON ml.project_id = p.id " +
        "WHERE p._name LIKE %:keyword% " +
        "OR ml.name LIKE %:keyword% " + 
        "OR ml.acreage LIKE %:keyword% " + 
        "OR ml.date_update LIKE %:keyword% " + 
        "OR ml.date_create LIKE %:keyword%", nativeQuery = true)
    Page<MasterLayout> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
