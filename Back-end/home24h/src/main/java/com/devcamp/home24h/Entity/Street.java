package com.devcamp.home24h.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "street")

public class Street {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message= "Vui lòng nhập tên đường")
    @Column(name = "_name")
    private String name;

    @Column(name = "_prefix")
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "_district_id")
    @JsonIgnore
    private District district;

    @Column(name = "_province_id")
    private int provinceId;

    @Transient
    private String provinceName;

    public Street() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public int getProvinceId() {
        return this.district.getProvinceId();
    }
    
    public String getProvinceName() {
        return this.district.getProvinceName();
    }

    public int getDistrictId() {
        return this.district.getId();
    }

    public String getDistrictName() {
        return this.district.getPrefix() + " " + this.district.getName();
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    
}
