package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.District;
import com.devcamp.home24h.Repository.DistrictRepository;
import com.devcamp.home24h.Service.DistrictService;

@RestController
@CrossOrigin
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @Autowired
    DistrictRepository iDistrict;

    // API lọc danh sách huyện theo điều kiện tên tỉnh, province Id
    @GetMapping("/districts")
    public ResponseEntity<Object> filterProvinces(@RequestParam(defaultValue = "0") int page, 
                                                  @RequestParam(defaultValue = "10") int size,
                                                  @RequestParam(defaultValue = "") String keyword,
                                                  @RequestParam(defaultValue = "id") String sortBy,
                                                  @RequestParam(defaultValue = "asc") String sortType ) {
        try {
            Page<District> districts = districtService.getListDistrictByPage(page, size, keyword, sortBy, sortType);
            return new ResponseEntity<>(districts, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
 

    @GetMapping("/provinces/{id}/districts")
    public ResponseEntity<List<District>> getListDistrictsByProvinceId(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iDistrict.getListDistrictsByProvinceId(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm layas ra district by district id
    @GetMapping("/districts/{id}")
    public ResponseEntity<District> getDistrictById(@PathVariable("id") int id) {
        try {
            return new ResponseEntity<>(iDistrict.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm thêm mới 1 quận, huyện
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/provinces/{provinceId}/districts")
    public ResponseEntity<District> createDistrict(@Valid @RequestBody District district, @PathVariable("provinceId") int provinceId) {
        try {
            District createdDistrict = districtService.createDistrict(district, provinceId);
            return new ResponseEntity<>(createdDistrict, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm sửa thông tin 1 quận, huyện
    @PutMapping("/provinces/{provinceId}/districts/{districtId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<District> updateDistrict(@Valid @RequestBody District district, @PathVariable("provinceId") int provinceId, @PathVariable("districtId") int districtId) {
        try {
            District updatedDistrict = districtService.updateDistrict(district, provinceId, districtId);
            return new ResponseEntity<>(updatedDistrict, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa 1 quận, huyện
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/districts/{id}")
    public ResponseEntity<District> deleteDistrict(@PathVariable("id") int id) {
        try {
            districtService.deleteDistrict(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

