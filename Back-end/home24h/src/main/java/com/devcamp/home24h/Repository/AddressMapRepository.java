package com.devcamp.home24h.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.AddressMap;

public interface AddressMapRepository extends JpaRepository<AddressMap, Integer>{
    @Query(value = "SELECT * FROM address_map WHERE " + 
        "address LIKE %:keyword% " + 
        "OR _lat LIKE %:keyword% " + 
        "OR _lng LIKE %:keyword%", nativeQuery = true)
    Page<AddressMap> findAllByKeyword(@Param("keyword") String keyword, Pageable pageable);
}
