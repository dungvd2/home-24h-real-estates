package com.devcamp.home24h.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.home24h.Entity.Realestate;
import com.devcamp.home24h.Entity.User;
import com.devcamp.home24h.Repository.RealestateRepository;
import com.devcamp.home24h.Repository.UserRepository;
import com.devcamp.home24h.Service.RealestateService;
import com.devcamp.home24h.security.UserPrincipal;

@RestController
@CrossOrigin
public class RealEstateController {
    @Autowired
    private RealestateRepository iRealestate;

    @Autowired
    private RealestateService realEstateService;

    @Autowired
    private UserRepository iUser;

    // API lấy ra danh sách 9 bất động sản mới đăng gần nhất (các loại)
    @GetMapping("/realestates/recently/all")
    public ResponseEntity<Object> getRecentRealestate() {
        try {
            List<Realestate> listRecent = realEstateService.getRecentRealestate();
            return ResponseEntity.ok(listRecent);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra danh sách 9 bất động sản mới đăng gần nhất (cần bán)
    @GetMapping("/realestates/recently/sale")
    public ResponseEntity<Object> getRecentRealestateForSale() {
        try {
            List<Realestate> listRecent = realEstateService.getRecentRealestateForSale();
            return ResponseEntity.ok(listRecent);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra danh sách 9 bất động sản mới đăng gần nhất (cho thuê)
    @GetMapping("/realestates/recently/rent")
    public ResponseEntity<Object> getRecentRealestateForRent() {
        try {
            List<Realestate> listRecent = realEstateService.getRecentRealestateForRent();
            return ResponseEntity.ok(listRecent);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra danh sách toàn bộ bđs ko phân trang
    @GetMapping("/realestates")
    public ResponseEntity<List<Realestate>> getAllRealEstates() {
        try {
            List<Realestate> realEstates = realEstateService.getAll();
            return new ResponseEntity<>(realEstates, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra thông tin bđs theo id dành cho trang admin
    @GetMapping("/realestates/{id}")
    public ResponseEntity<Realestate> getRealEstateByID(@PathVariable("id") int id) {
        try {
            Realestate realEstate = iRealestate.findById(id).orElse(null);
            if (realEstate != null) {
                realEstateService.setNameForCollumn(realEstate);
            }
            return new ResponseEntity<>(realEstate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra thông tin bất động sản vừa đăng của người dùng
    // CHỉ có người đăng mới xem đc
    @GetMapping("/realestates/get/{id}")
    public ResponseEntity<Realestate> getRealestateOfCustomer(@PathVariable("id") int id) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            User user = iUser.findByUsername(userPrincipal.getUsername());
            Realestate realestate = iRealestate.findById(id).orElse(null);
            if(!realestate.getCustomerId().equals(user.getId())) {
                return ResponseEntity.badRequest().body(null);
            }
            realEstateService.setNameForCollumn(realestate);
            return ResponseEntity.ok(realestate);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra thông tin bđs theo id dành cho trang customer
    @GetMapping("/realestates/detail/{id}")
    public ResponseEntity<Realestate> getRealEstateByIdForCustomer(@PathVariable("id") int id) {
        try {
            Realestate realEstate = iRealestate.findById(id).orElse(null);
            if (realEstate != null && realEstate.getStatus() == 1) {
                realEstateService.setNameForCollumn(realEstate);
            }
            return new ResponseEntity<>(realEstate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lọc ra thông tin bđs ở trang admin theo các tiêu chí
    @GetMapping("/realestates/filter")
    public ResponseEntity<Page<Realestate>> filterRealEstate(@RequestParam(defaultValue = "") String type,
            @RequestParam(defaultValue = "") String status,
            @RequestParam(defaultValue = "") String request,
            @RequestParam(defaultValue = "0") int provinceId,
            @RequestParam(defaultValue = "0") int districtId,
            @RequestParam(defaultValue = "0") int wardId,
            @RequestParam(defaultValue = "0") Long min,
            @RequestParam(defaultValue = "999999999999") Long max,
            @RequestParam(defaultValue = "-1") int customerId,
            @RequestParam(defaultValue = "-1") int projectId,
            @RequestParam(defaultValue = "asc") String sortType,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        try {
            Page<Realestate> realEstates = realEstateService.filterRealEstateAdminPage(status, request, type,
                    provinceId, districtId, wardId, customerId, projectId, min, max, sortBy, sortType, page, size);
            for (Realestate realestate : realEstates) {
                realEstateService.setNameForCollumn(realestate);
            }
            return new ResponseEntity<>(realEstates, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tìm kiếm danh sách bất động sản có phân trang theo các tiêu chí ở trang
    // dành cho khách hàng
    @GetMapping("/realestates/search")
    public ResponseEntity<Page<Realestate>> searchRealestate(
                                                        @RequestParam(name = "province", defaultValue = "") String province,
                                                        @RequestParam(name = "district", defaultValue = "") String district,
                                                        @RequestParam(name = "ward", defaultValue = "") String ward,
                                                        @RequestParam(name = "request", defaultValue = "") String request,
                                                        @RequestParam(name = "type", defaultValue = "") String type,
                                                        @RequestParam(name = "pricemin", defaultValue = "0") Long pricemin,
                                                        @RequestParam(name = "pricemax", defaultValue = "999999999999") Long pricemax,
                                                        @RequestParam(name = "areamin", defaultValue = "0") double areamin,
                                                        @RequestParam(name = "areamax", defaultValue = "999999") double areamax,
                                                        @RequestParam(name = "bedroommin", defaultValue = "0") int bedroommin,
                                                        @RequestParam(name = "bedroommax", defaultValue = "9999") int bedroommax,
                                                        @RequestParam(name = "bathroommin", defaultValue = "0") int bathroommin,
                                                        @RequestParam(name = "bathroommax", defaultValue = "9999") int bathroommax,
                                                        @RequestParam(name = "direction", defaultValue = "") String direction,
                                                        @RequestParam(name = "sortBy", defaultValue = "") String sortBy,
                                                        @RequestParam(name = "sortType", defaultValue = "") String sortType,
                                                        @RequestParam(name = "page", defaultValue = "0") int page,
                                                        @RequestParam(name = "size", defaultValue = "8") int size) {
        try {
            Page<Realestate> result = realEstateService.searchRealEstateCustomerPage(province, district, ward, request, type, pricemin, pricemax, areamin, areamax, bedroommin, bedroommax, bathroommin, bathroommax, direction, sortBy, sortType, page, size);
            // Thêm tên xã, huyện tỉnh, đường dựa vào id
            for (Realestate realestate : result) {
                realEstateService.setNameForCollumn(realestate);
            }
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API Lấy ra danh sách các bất động sản mà khách hàng đã thêm vào có phân trang
    @GetMapping("/realestates/createby")
    public ResponseEntity<Page<Realestate>> getListRealestateCreateByCustomer(@RequestParam(defaultValue = "") String status, 
                                                                            @RequestParam(defaultValue = "") String sortBy,
                                                                            @RequestParam(defaultValue = "") String sortType,
                                                                            @RequestParam(defaultValue = "0") int page,
                                                                            @RequestParam(defaultValue = "9") int size) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            if(userPrincipal == null) {
                return ResponseEntity.badRequest().body(null);
            }
            Page<Realestate> listRealestateOfCustomer = realEstateService.getListRealestateCreateByUser(userPrincipal.getUserId(), status, sortBy, sortType, page, size);
            for(Realestate realestate: listRealestateOfCustomer) {
                realEstateService.setNameForCollumn(realestate);
            }
            return ResponseEntity.ok(listRealestateOfCustomer);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }                                                                        
    }

    // API tạo mới bất động sản cho khách hàng
    @PostMapping("/realestates/create")
    public ResponseEntity<Object> createRealestateByCustomer(@Valid @RequestBody Realestate realEstate) {
        try {
            String messageValidate = realEstateService.validateRealestate(realEstate);
            if(!messageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(messageValidate);
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            if(userPrincipal == null) {
                return ResponseEntity.badRequest().body("Bạn phải đăng nhập mới có thể đăng tin");
            }
            User user = iUser.findByUsername(userPrincipal.getUsername());
            Realestate createdRealEstate = realEstateService.createRealEstateByCustomer(realEstate, user.getId());
            return new ResponseEntity<>(createdRealEstate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại sau");
        }
    }
    
    
    // API tạo mới bđs cho admin và môi giới
    @PostMapping("/realestates")
    public ResponseEntity<Object> createRealEstate(@Valid @RequestBody Realestate realEstate) {
        try {
            String messageValidate = realEstateService.validateRealestate(realEstate);
            if(!messageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(messageValidate);
            }
            Realestate createdRealEstate = realEstateService.createRealEstate(realEstate);
            return new ResponseEntity<>(createdRealEstate, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra, vui lòng thử lại sau");
        }
    }

    // APi cập nhật thông tin bđs cho người dùng, chỉ có người đăng mới cập nhật được
    @PutMapping("/realestates/update/{id}")
    public ResponseEntity<Object> updateRealestateByCustomer(@PathVariable("id") int id, @Valid @RequestBody Realestate pRealEstate) {
        try {
            String messageValidate = realEstateService.validateRealestate(pRealEstate);
            if(!messageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(messageValidate);
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            Realestate realestate = iRealestate.findById(id).orElse(null);
            if(!realestate.getCustomerId().equals(userPrincipal.getUserId()) || realestate == null) {
                return ResponseEntity.badRequest().body("Có lỗi xảy ra, vui lòng thử lại sau");
            }
            realEstateService.updateRealEstateByCustomer(realestate, pRealEstate);
            return ResponseEntity.ok("Cập nhật thành công!");

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>("Có lỗi xảy ra, vui lòng thử lại sau", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // API sửa, cập nhật thông tin 1 bđs
    @PutMapping("/realestates/{id}")
    public ResponseEntity<Object> updateRealEstate(@PathVariable("id") int id, @Valid @RequestBody Realestate realEstate) {
        try {
            String messageValidate = realEstateService.validateRealestate(realEstate);
            if(!messageValidate.equals("ok")) {
                return ResponseEntity.badRequest().body(messageValidate);
            }
            Realestate updatedRealEstate = realEstateService.updateRealEstate(realEstate, id);
            return new ResponseEntity<>(updatedRealEstate, HttpStatus.OK);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API xóa thông tin bđs bởi Admin và quản lý
    @DeleteMapping("/realestates/{id}")
    public ResponseEntity<HttpStatus> deleteRealEstate(@PathVariable("id") int id) {
        try {
            realEstateService.deleteRealEstate(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API Xóa thông tin bđs bởi chính người dùng đã đăng
    @DeleteMapping("/realestates/remove/{id}")
    public ResponseEntity<Object> deleteRealestateByUser(@PathVariable("id") int id) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            Realestate realestate = iRealestate.findById(id).orElse(null);
            if(realestate.getCustomerId() != userPrincipal.getUserId()) {
                return ResponseEntity.badRequest().body("Chỉ có người đăng và quản trị viên mới có quyền xóa!");
            }
            realEstateService.deleteRealEstate(id);
            return ResponseEntity.ok("Xóa bài đăng thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>("Có lỗi xảy ra, vui lòng thử lại sau!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API thay đổi trạng thái của bất động sản
    @PutMapping("/realestates/change-status/{id}")
    public ResponseEntity<Object> changeRealEstateStatus(@PathVariable("id") Integer id,
            @RequestParam("status") Integer status) {
        try {
            realEstateService.changeRealestateStatus(id, status);
            return ResponseEntity.ok("Thay đổi trạng thái thành công!");
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.internalServerError().body("Có lỗi xảy ra, vui lòng thử lại sau");
        }
    }
}
