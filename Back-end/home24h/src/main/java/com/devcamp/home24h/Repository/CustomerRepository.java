package com.devcamp.home24h.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.home24h.Entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer>{

    // Hàm lọc dữ liệu khách hàng theo tên, số điện thoại, và kiểu sắp xếp tùy tình huống mà sắp xếp theo số luownnj tăng dần hay giảm dần
    @Query(value =  "SELECT c.* FROM customers c " + 
                    "LEFT JOIN ( " + 
                    "SELECT customer_id, COUNT(*) as num_realestate " +
                    "FROM realestate GROUP BY customer_id " + 
                    ") r ON c.id = r.customer_id " +
                    "WHERE contact_name LIKE %:name% AND mobile LIKE %:phone% " + 
                    "ORDER BY " + 
                    "CASE WHEN :type = 'default' THEN c.id END, " + 
                    "CASE WHEN :type = 'ASC' THEN num_realestate END ASC, " + 
                    "CASE WHEN :type = 'DESC' THEN num_realestate END DESC", nativeQuery = true)
    List<Customer> filterCustomers(@Param("name") String name, @Param("phone") String phone, @Param("type") String type);
}
