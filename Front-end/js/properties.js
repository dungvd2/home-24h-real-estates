// Hàm load ra danh sách các quận tương ứng của tỉnh
function callApiToLoadListDistrict() {
    let vProvinceId = $("#select-province").val();
    if (!vProvinceId) {
      resetDistrictFilter();
      resetStreetFilter();
      resetWardFilter();
      resetSelectProject();
      return;
    }
    $.ajax({
      type: "GET",
      url: gBASE_URL_PROVINCE + vProvinceId + "/districts",
      async: false,
      success: function (paramRes) {
        loadDataToSelectDistrict(paramRes);
      },
      error: function (paramError) {
        console.log(paramError.status);
      } 
    });

  }

  // Hàm load dữ liệu vào select district
  function loadDataToSelectDistrict(paramData) {
    let vSelectDistrictFilter = $("#select-district");
    vSelectDistrictFilter.empty();
    vSelectDistrictFilter.selectric('destroy');
    $("<option>", {
      text: "Quận, huyện",
      value: ""
    }).appendTo(vSelectDistrictFilter);
    
    for (let bI = 0; bI < paramData.length; bI++) {
      $("<option>", {
        text: paramData[bI].prefix + " " + paramData[bI].name,
        value: paramData[bI].id
      }).appendTo(vSelectDistrictFilter);
    }
    vSelectDistrictFilter.selectric();
  }   

  // Hàm load ra danh sách các xã tương ứng của huyện
  function callApiToLoadListWard() {
    let vDistrictId = $("#select-district").val();
    if (!vDistrictId) {
      resetWardFilter();
      resetStreetFilter();
      resetSelectProject();
      return;
    }
    $.ajax({
      type: "GET",
      url: gBASE_URL_DISTRICT + vDistrictId + "/wards",
      async: false,
      success: function (paramRes) {
        loadDataToSelectWard(paramRes);
      },
      error: function (paramError) {
        console.log(paramError.status);
      }
    });

  }

  // Hàm load dữ liệu vào select district
  function loadDataToSelectWard(paramData) {
    let vSelectWard = $("#select-ward");
    vSelectWard.empty();
    vSelectWard.selectric('destroy');
    $("<option>", {
      text: "Phường, xã",
      value: ""
    }).appendTo(vSelectWard);
    for (let bI = 0; bI < paramData.length; bI++) {
      $("<option>", {
        text: paramData[bI].prefix + " " + paramData[bI].name,
        value: paramData[bI].id
      }).appendTo(vSelectWard);
    }
    vSelectWard.selectric();
  }


  // Hàm reset select quận huyện
  // Output: Xóa hết các option của select quận huyện
  function resetDistrictFilter() {
      let vSelectDistrictFilter = $("#select-district");
      vSelectDistrictFilter.empty();
      vSelectDistrictFilter.selectric('destroy');
      $("<option>", {
          value: "",
          text: "Quận, huyện"
      }).appendTo(vSelectDistrictFilter);
      vSelectDistrictFilter.selectric();
  }

  // Hàm reset select phường, xã
  // Output: Xóa hết các option của select phường, xã
  function resetWardFilter() {
      let vSelectWard = $("#select-ward");
      vSelectWard.empty();
      vSelectWard.selectric('destroy');
      $("<option>", {
          value: "",
          text: "Phường, xã"
      }).appendTo(vSelectWard);
      vSelectWard.selectric();
  }


  // Hàm reset select đường
  // Output: Xóa hết các option của select đường
  function resetStreetFilter() {
    let vSelectStreet = $("#select-street");
    vSelectStreet.empty();
    vSelectStreet.selectric('destroy');
    $("<option>", {
        value: "",
        text: "Đường"
    }).appendTo(vSelectStreet);
    vSelectStreet.selectric();
}

// Hàm reset select dự án
  // Output: Xóa hết các option của select dự án
  function resetSelectProject() {
    let vSelectProject = $("#select-project");
    vSelectProject.empty();
    vSelectProject.selectric('destroy');
    $("<option>", {
        value: "",
        text: "Dự án"
    }).appendTo(vSelectProject);
    vSelectProject.selectric();
}

  // Hàm load ra danh sách các xã tương ứng của huyện
  function callApiToLoadListStreet() {
    let vDistrictId = $("#select-district").val();
    if (vDistrictId == "") {
      resetWard();
      resetStreet();
      resetProject();
      return;
    }
    $.ajax({
      type: "GET",
      url: gBASE_URL_DISTRICT + vDistrictId + "/streets",
      async: false,
      success: function (paramRes) {
        loadDataToSelectStreet(paramRes);
      },
      error: function (paramError) {
        console.log(paramError.status);
      }
    });

  }

  // Hàm load dữ liệu vào select street
  function loadDataToSelectStreet(paramData) {
    let vSelectStreet = $("#select-street");
    vSelectStreet.empty();
    vSelectStreet.selectric('destroy');
    $("<option>", {
      text: "Đường",
      value: ""
    }).appendTo(vSelectStreet);
    for (let bI = 0; bI < paramData.length; bI++) {
      $("<option>", {
        text: paramData[bI].prefix + " " + paramData[bI].name,
        value: paramData[bI].id
      }).appendTo(vSelectStreet);
    }

    vSelectStreet.selectric();
  }


    // Hàm gọi API để lấy ra danh sách dự án
    // Output: Một mảng gồm tất cả các dự án
    function callApiToLoadListProject() {
      let vDistrictId = $("#select-district").val();
    if (vDistrictId == "") {
      resetWard();
      resetStreet();
      resetProject();
      return;
    }
        $.ajax({
          type: "GET",
          url: gBASE_URL_PROJECT + "districts/" + vDistrictId,
          async: false,
          success: function (paramRes) {
            loadDataToSelectProject(paramRes);
          },
          error: function (paramError) {
            console.log(paramError.status);
          }
        });
      }
  
      // Hàm load dữ liệu danh sách project vào select project
      // Input: Danh sách project
      // Output: Tạo các option và đưa vào select project
      function loadDataToSelectProject(paramProjects) {
        let vSelectProject = $("#select-project");
        vSelectProject.empty();
        vSelectProject.selectric('destroy');
        $("<option>", {
            text: "Dự án",
            value: ""
        }).appendTo(vSelectProject);
        for (let bI = 0; bI < paramProjects.length; bI++) {
          $("<option>", {
            text: paramProjects[bI].name,
            value: paramProjects[bI].id
          }).appendTo(vSelectProject);
        }
        vSelectProject.selectric();
      }