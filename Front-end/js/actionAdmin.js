

// Hàm lấy các thông số để load dữ liệu ra bảng
function getParamToLoadData() {
  let vInputPage = $(".inp-page-number").val();
  let vSize = $(".select-size").val();
  let vKeyword = $("#inp-keyword").val();
  let vUrl = gBASE_URL + "?page=" + (vInputPage - 1) + "&size=" + vSize + "&keyword=" + vKeyword + "&sortBy=" + gSortBy + "&sortType=" + gSortType;
  loadTable(vUrl);
}

// Hàm tự động thay đổi các tùy biến của trang sao cho hợp lí theo số trang hiện tại
// Input: Dữ liệu trang trả về từ API
// Output: Thay đổi các nút next, previous, last, first disable or able sao cho hợp lý bằng addclass, và removeclass disabled, thay đổi size, thông số hiện tại của bảng
function flexiblePaginationByCurrentPage(paramTableData) {
  let vTotalPage = paramTableData.totalPages;
  gNumberPageOfTable = vTotalPage;
  let vTotalElement = paramTableData.totalElements;
  let vCurrentPage = paramTableData.number;
  gCurrentPage = vCurrentPage;
  let vSize = paramTableData.size;
  let vStartNumber = vCurrentPage * vSize;
  let vEndNumber = Math.min(vStartNumber + vSize, vTotalElement);
  if (vCurrentPage < 1) {
    $(".page-first").addClass("disabled");
    $(".page-previous").addClass("disabled");
  }
  else {
    $(".page-first").removeClass("disabled");
    $(".page-previous").removeClass("disabled");
  }

  if (vCurrentPage == vTotalPage - 1) {
    $(".page-next").addClass("disabled");
    $(".page-last").addClass("disabled");
  }
  else {
    $(".page-next").removeClass("disabled");
    $(".page-last").removeClass("disabled");
  }

  $(".inp-page-number").val(vCurrentPage + 1);
  $(".inp-page-number").attr("max", vTotalPage);
  $(".select-size").val(vSize);
  let vNumberData = "Record " + (vStartNumber + 1) + " to " + vEndNumber + " of " + vTotalElement;
  $(".number-data").html(vNumberData);
}



// Hàm xử lý sự kiện nhấn nút trở về trang đầu tiên
function onBtnNavigationToFirstPageClick(paramBtn) {
  if (!$(paramBtn).hasClass("disabled")) {
    let vSize = $(".select-size").val();
    let vKeyword = $("#inp-keyword").val();
    let vUrl = gBASE_URL + "?page=0&size=" + vSize + "&keyword=" + vKeyword + "&sortBy=" + gSortBy + "&sortType=" + gSortType;
    loadTable(vUrl);
  }
}

// Hàm xử lý sự kiện nhấn nút lùi 1 trang
function onBtnNavigationToPreviousPageClick(paramBtn) {
  if (!$(paramBtn).hasClass("disabled")) {
    let vCurrentPage = $(".inp-page-number").val();
    let vSize = $(".select-size").val();
    let vKeyword = $("#inp-keyword").val();
    let vUrl = gBASE_URL + "?page=" + (vCurrentPage - 2) + "&size=" + vSize + "&keyword=" + vKeyword + "&sortBy=" + gSortBy + "&sortType=" + gSortType;
    loadTable(vUrl);
  }
}

// Hàm xử lý sự kiện nhấn nút tiến 1 trang
function onBtnNavigationToNextPageClick(paramBtn) {
  if (!$(paramBtn).hasClass("disabled")) {
    let vCurrentPage = $(".inp-page-number").val();
    let vSize = $(".select-size").val();
    let vKeyword = $("#inp-keyword").val();
    let vUrl = gBASE_URL + "?page=" + vCurrentPage + "&size=" + vSize + "&keyword=" + vKeyword + "&sortBy=" + gSortBy + "&sortType=" + gSortType;
    loadTable(vUrl);
  }
}

// Hàm xử lý sự kiện nhấn nút đến trang cuối cùng
function onBtnNavigationToLastPageClick(paramBtn) {
  if (!$(paramBtn).hasClass("disabled")) {
    let vSize = $(".select-size").val();
    let vKeyword = $("#inp-keyword").val();
    let vUrl = gBASE_URL + "?page=" + (gNumberPageOfTable - 1) + "&size=" + vSize + "&keyword=" + vKeyword + "&sortBy=" + gSortBy + "&sortType=" + gSortType;
    loadTable(vUrl);
  }
}

// Hàm xử lý sự kiện thay đổi số trang ở input trang
function onChangePageNumber(paramInputPage) {
  let vInputPage = $(paramInputPage).val();
  if (vInputPage < 1) {
    alert("Số trang bắt đầu từ 1!");
    $(paramInputPage).val(1);
    return;
  }
  let vInputPageMax = $(paramInputPage).attr("max");
  if (vInputPage > vInputPageMax) {
    $(paramInputPage).val(gCurrentPage + 1);
    alert("Trang nhập vào phải nhỏ hơn hoặc bằng số trang hiện có là " + vInputPageMax + " trang!");
    return;
  }
  vInputPage = $(paramInputPage).val();
  let vSize = $(".select-size").val();
  let vKeyword = $("#inp-keyword").val();
  let vUrl = gBASE_URL + "?page=" + (vInputPage - 1) + "&size=" + vSize + "&keyword=" + vKeyword + "&sortBy=" + gSortBy + "&sortType=" + gSortType;
  loadTable(vUrl);
}

// Hàm xử lý sự kiện thay đổi size
function onChanegSizeOfTable(paramSelectSize) {
  resetFilterData();
  let vCurrentPage = $(".inp-page-number").val();
  let vSize = $(paramSelectSize).val();
  let vKeyword = $("#inp-keyword").val();
  let vUrl = gBASE_URL + "?page=" + (vCurrentPage - 1) + "&size=" + vSize + "&keyword=" + vKeyword + "&sortBy=" + gSortBy + "&sortType=" + gSortType;
  loadTable(vUrl);
}

// Hàm chuyển giờ giấc sang giờ Việt Nam
function convertTimeToVietNamTime(paramDateTime) {
  let vUtcDate = new Date(paramDateTime); // Tạo đối tượng Date từ chuỗi ngày tháng
  let vVietNamOffSet = 7 * 60; // Độ lẹch múi giờ GMT +7

  // Chuyển từ múi giờ UTC sang VIETNAM
  let vVietNamTime = new Date(vUtcDate.getTime() + vVietNamOffSet * 60000);

  // Định dạng lại theo đúng kiểu ngày tháng việt nam
  let vFormattedDate = vVietNamTime.toISOString(/T|\.\d{3}Z/g, " ");
  // Bây giờ ngày tháng sẽ có kiểu 2023-08-02 20:32:03 sẽ tiếp tục để chuyển thành 20:32:03 02-08-2023
  // Tách ra thành phần ngày tháng và giờ
  let vDate = vFormattedDate.substring(0, 10); //2023-08-02
  let vTime = vFormattedDate.substr(11, 8); //20:32:03
  // Dùng split để tách ngày ra thành từng phần bởi split("-");
  let vArrDate = vDate.split("-");
  vDate = vArrDate[2] + "-" + vArrDate[1] + "-" + vArrDate[0];

  // Gộp ngày tháng lại và trả về
  return vDate;

}

//Xử lý object trả về khi login thành công
function responseHandler(data) {
  //Lưu token vào cookie trong 1 ngày
  setCookie("token", data, 1);
  setTimeout(function () {
    // Navigate to index.html
    window.location.href = "addressmap.html";
  }, 1000);
}

//Hàm setCookie đã giới thiệu ở bài trước
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//Hàm get Cookie đã giới thiệu ở bài trước
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}


// Hàm hiển thông báo  thành công
function alertSuccess(paramNotification) {
  gToast.fire({
    icon: 'success',
    title: paramNotification
  });
}

// Hàm thông báo lỗi validate theo toast
function alertWarning(paramTitile) {
  gToast.fire({
    icon: 'warning',
    title: "" + paramTitile
  });
}

// Hàm hiển thông báo có lỗi trong quá trình đăng ký
function alertError(paramTitile) {
  gToast.fire({
    icon: 'error',
    title: paramTitile
  });
}


// Hàm xử lý sự kiện nhấn vào tiêu đề bảng thì sort dữ liệu
// Input: Thông tin dòng tiêu đề được bấm
// Output: Nếu bấm vào lần đầu thì thêm awefont biểu tượng tăng dần, lần 2 thì giảm dần và lại tăng dần nếu lần 3
// Từ đó lấy ra thông số từ pos(nếu ko có thì ko lấy ra được thông số) và gọi hàm getDataByparam để lấy ra dữ liệu đã sort
function onSortDataByThClick(paramTh) {
  let vPosTh = $(paramTh).attr("pos");
  if (!vPosTh) {
    return;
  }
  let vSpanTypeSort = $(paramTh).find("span");
  let vType = vSpanTypeSort.attr("type");
  let vAllSpan = $("th span");
  vAllSpan.remove();
  vSpanTypeSort.remove();
  if (!vType || vType == "desc") {
    vSpanTypeSort = ` <span type="asc"><i class="fas fa-sort-up"></i></span>`
    vType = "asc";
  }
  else {
    vSpanTypeSort = ` <span type="desc"><i class="fas fa-sort-down"></i></span>`
    vType = "desc";
  }
  gSortType = vType;
  gSortBy = gListColumns[vPosTh];
  $(paramTh).append(vSpanTypeSort);
  getParamToLoadData();
}


// Hàm để reset dữ liệu khi thực hiện các thao tác
// Output: Đưa số trang về 1, đưa kiểu sắp xếp, dữ liệu sắp xếp về rỗng, xóa bỏ các dấu sort trên th
function resetFilterData() {
  $(".inp-page-number").val(1);
  gSortBy = "";
  gSortType = "";
  let vAllSpan = $("th span");
  vAllSpan.remove();
}


// Hàm gọi API để tạo mã bảo vệ
function callApiToCreatPassCode(paramRequestContext) {
  $.ajax({
    type: "GET",
    url: gBASE_URL_USER + "create-passcode?requestContext=" + paramRequestContext,
    headers: gHEADERS,
    success: function (paramRes) {
      console.log(paramRes);
    },
    error: function (paramError) {
      console.log(paramError.responseText);
    }
  });
}

// Hàm kiểm tra email có đúng định dạng hay không
// Input: Email đăng ký
// Output: Sử dủng regex để check, trả về true nếu hợp lệ còn false nếu không
function validateEmail(paramEmail) {
  // Sử dụng regex để kiểm tra địa chỉ email
  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
  if (emailPattern.test(paramEmail)) {
    return true;
  }
  return false;
}

// Hàm kiểm tra số điện thoại có đúng định dạng hay không
// Input: Số điện thoại đăng ký
// Output: Sử dủng regex để check, trả về true nếu hợp lệ còn false nếu không
function validatePhoneNumber(paramPhoneNumber) {
  var vPhoneNumberPattern = /^0[0-9]{9}$/;
  if (vPhoneNumberPattern.test(paramPhoneNumber)) {
    return true;
  }
  return false;
}

// Hàm validate mật khẩu phải có từ 6 kí tự trở lên bao gồm cả chữ lẫn số
// Input: Mật khẩu người dùng
// Output: Sử dủng regex để check, trả về true nếu hợp lệ còn false nếu không
function validatePassword(paramPassword) {
  var passwordPattern = /^(?=.*[a-zA-Z])(?=.*[0-9]).{6,}$/;
  return passwordPattern.test(paramPassword);
}

// Hàm gọi API để check xem token có hợp lệ hay không
// Input: Token 
// Output: Nếu hợp lệ gọi hàm hello user để xử lý, nếu không in lỗi ra console
function callApiToHelloUser() {
  let vHeader = {
    Authorization: "Token " + gTOKEN
  }
  $.ajax({
    type: "GET",
    url: gBASE_URL_USER + "hello1",
    headers: vHeader,
    success: function (paramRes) {
      helloUser(paramRes);
    },
    error: function (paramError) {
      window.location.href = "login.html";
      console.log(paramError.status);
    }
  });
}

// Hàm xử lý sau khi check token trả về
// Input: Thông tin user lấy ra từ token trả về từ API
// Ouput: Tùy theo role mà đưa đến từng vị trí thích hợp
function helloUser(paramUser) {
  let vUserRole = paramUser.authorities[0];
  let vUsername = paramUser.username;
  $("#span-username-login").html(vUsername);
  if (vUserRole == "ROLE_CUSTOMER") {
    window.location.href = "index.html";
  }
  else if (vUserRole == "ROLE_HOME_SELLER") {
    $(".nav-navigate").attr("hidden", false);
    window.location.href = "addressmap.html";
  }
  else if (vUserRole == "ROLE_ADMIN") {
    $(".nav-navigate").attr("hidden", false);
    $(".nav-navigate-admin").attr("hidden", false);
    $(".btn-add").attr("hidden", false);
  }

  $(".nav-login").attr("hidden", true);
  $(".nav-view").attr("hidden", false);
} 