// Hàm trả về địa chỉ gồm xã huyện tỉnh của bất động sản
    // Input: Đối tượng bất động sản
    // Output: Trả ra địa chính gồm xã, huyện, tỉnh
    function transferLocation(paramRealEstate) {
        let vResult = "";
        let vProvince = "";
        let vDistrict = "";
        let vWard = "";
        // Province name sẽ có dạng Tp.HCM, SG cần loại bỏ mã ở đằng sau đi dùng split tách ra bằng dấu phẩy
        if (paramRealEstate.provinceName) {
            let vProvinceWithCode = paramRealEstate.provinceName;
            let vArrProvince = vProvinceWithCode.split(",");
            vProvince = vArrProvince[0];
        }
        if (paramRealEstate.districtName) {
            vDistrict = paramRealEstate.districtName;
        }
        if (paramRealEstate.wardName) {
            vWard = paramRealEstate.wardName;
        }

        if (vWard != "") {
            vResult += vWard;
        }

        if (vDistrict != "") {
            vResult += ", " + vDistrict;
        }

        if (vProvince != "") {
            vResult += ", " + vProvince;
        }
        return vResult;
    }


    // Hàm trả về địa gồm số nhà, đường, xã huyện tỉnh của bất động sản
    // Input: Đối tượng bất động sản
    // Output: Trả ra địa chính gồm xã, huyện, tỉnh
    function transferFullLocation(paramRealEstate) {
        let vResult = "";
        let vProvince = "";
        let vDistrict = "";
        let vWard = "";
        let vStreet = "";
        let vAddress = "";
        // Province name sẽ có dạng Tp.HCM, SG cần loại bỏ mã ở đằng sau đi dùng split tách ra bằng dấu phẩy
        if (paramRealEstate.provinceName) {
            let vProvinceWithCode = paramRealEstate.provinceName;
            let vArrProvince = vProvinceWithCode.split(",");
            vProvince = vArrProvince[0];
        }
        if (paramRealEstate.districtName) {
            vDistrict = paramRealEstate.districtName;
        }
        if (paramRealEstate.wardName) {
            vWard = paramRealEstate.wardName;
        }

        if(paramRealEstate.streetName) {
            vStreet = paramRealEstate.streetName;
        }

        if(paramRealEstate.address) {
            vAddress = paramRealEstate.address;
        }

        if(vAddress != "") {
            vResult += vAddress + ", ";
        }

        if(vStreet != "") {
            vResult += vStreet + ", ";
        }

        if (vWard != "") {
            vResult += vWard + ", ";
        }

        if (vDistrict != "") {
            vResult += vDistrict + ", ";
        }

        if (vProvince != "") {
            vResult += vProvince;
        }
        return vResult;
    }

    // Hàm chuyển đổi giá trị tiền tệ sang kiểu VNĐ cho dễ đọc
    // Input: Đối tượng bất động sản
    // Output: Dựa theo kiểu mua hay thuê, tiền thì quy đổi ra tỉ, triệu thêm tiền tố tháng nếu là cho thuê
    function transferToVNDPrice(paramRealEstate) {
        let vResult = "";
        let vMonth = "";
        let vPrice = 1.0;
        if (paramRealEstate.request == 0) {
            vPrice = paramRealEstate.price;
        }
        else if (paramRealEstate.request == 1) {
            vPrice = paramRealEstate.priceRent;
            vMonth = "/tháng";
        }

        let vBeforeNumber = "Đồng";
        if (vPrice >= 1000000000) {
            vBeforeNumber = "Tỷ";
            vPrice /= 1000000000;
        }
        else if (vPrice >= 1000000) {
            vBeforeNumber = "Triệu";
            vPrice /= 1000000;
        }

        vResult = vPrice + " " + vBeforeNumber + vMonth;
        return vResult;
    }

    // Hàm lấy số lượng phòng
    // Input: Số lượng phòng 
    // Output: Nếu có thì trả về số nếu null thì trả về ...
    function getNumberRoom(paramNumberRoom) {
        if (!paramNumberRoom) {
            return "...";
        }
        return paramNumberRoom;
    }

    // Hàm trả về hướng của nhà
    // Input: Số hướng, theo quy ước, 0,1,2...
    // Output: Nếu null thì trả về ... nếu có thì trả về vd 0: đông, tây...
    function getDirectionOfRealEstate(paramNumberDirection) {
        let vResult = "...";
        if (paramNumberDirection == "0") {
            vResult = "Đông";
        }

        if (paramNumberDirection == "1") {
            vResult = "Tây";
        }

        if (paramNumberDirection == "2") {
            vResult = "Nam";
        }

        if (paramNumberDirection == "3") {
            vResult = "Bắc";
        }

        if (paramNumberDirection == "4") {
            vResult = "Tây bắc";
        }

        if (paramNumberDirection == "5") {
            vResult = "Tây nam";
        }

        if (paramNumberDirection == "6") {
            vResult = "Đông bắc";
        }

        if (paramNumberDirection == "7") {
            vResult = "Đông nam";
        }

        return vResult;
    }

    // Hàm chuyển đổi từ cần bán cho thuê (vì là kiểu số quy định 0 là bán, 1 là cho thuê)
    // Input: Yêu cầu 0, 1
    // Output: 0 bán, 1 cho thuê
    function transferRequest(paramRequest) {
        if (paramRequest == 0) {
            return "Cần bán";
        }
        return "Cho thuê";
    }

    // Hàm gọi API để load ra danh sách tất cả bất động sản đăng gần đây (cả cho thuê và bán)
    // Output: Mảng gồm 9 đối tượng bất động sản mới đăng gần đây nhất
    function callApiToLoadRecentRealestate() {
        $.ajax({
            type: "GET",
            url: gBASE_URL_REALESTATE + "recently/all",
            success: function (paramRes) {
                let vSlideRecentRealestate = $(".swiper-wrapper");
                loadListRecentRealesate(vSlideRecentRealestate, paramRes);
            },
            error: function (paramError) {
                console.log(paramError.status);
            }
        });
    }

    // Hàm load danh sách bất động sản gần đây ra form
    // Input: Tạo 1 swiper sau đó thêm vào slide-wraper, sau đó mỗi lần thêm vào dữ liệu 2 bđs
    // Output: Các bất động sản mới đăng sẽ được load ra form
    function loadListRecentRealesate(paramTag, paramListRealestate) {

        for (let bI = 0; bI < 6; bI = bI + 2) {
            // Tạo 1 thẻ slide thêm vào slide box
            let vSlide = $(paramTag).find(".swiper-slide").eq(bI/2 + 1);
            loadRecentRealEstateBox(vSlide, paramListRealestate[bI], paramListRealestate[bI+1]);
        }
    }

    // Hàm load ra form slide của bất động sản gần đây nhất
    // Input: thẻ slide để chứ thông tin bđs, 2 thông tin bất động sản gần nhất
    // Output: Load dữ liệu ra slide
    function loadRecentRealEstateBox(paramTag, paramRealEstate1, paramRealEstate2) {
        // Lấy ra hình ảnh đầu tiên của bđs1
        let vArrPhoto1 = paramRealEstate1.photo.split(",");
        let vFirstPhoto1 = vArrPhoto1[0];
        // Lấy ra hình ảnh đầu tiên của bđs2
        let vArrPhoto2 = paramRealEstate2.photo.split(",");
        let vFirstPhoto2 = vArrPhoto2[0];
        let vSlideBox = `<div
                                class="overflow-hidden rounded-md drop-shadow-[0px_2px_3px_rgba(0,0,0,0.1)] bg-[#FFFDFC] text-center mb-[40px]">
                                                    <div class="relative">
                                                        <a href="properties-details.html?id=`+ paramRealEstate1.id +`" class="block">
                                                            <img src="asset/img/realestate/`+ vFirstPhoto1 +`"
                                                                class="w-full h-full" loading="lazy" width="370"
                                                                height="266" alt="@@title">
                                                        </a>

                                                    </div>

                                                    <div class="pt-[15px] pb-[20px] px-[20px] text-left">
                                                        <h3 style="height: 3.5em;overflow: hidden;text-overflow: ellipsis;"><a href="properties-details.html?id=`+ paramRealEstate1.id +`"
                                                                class="font-lora leading-tight text-[18px] text-primary">`+ paramRealEstate1.title +`</a></h3>
                                                        <h4 class="leading-none"><a href="properties-details.html?id=`+ paramRealEstate1.id +`"
                                                                class="font-light text-[14px] leading-[1.75] text-primary underline">`+ transferLocation(paramRealEstate1) +`</a></h4>
                                                        <ul class="mt-[10px]">
                                                            <li class="flex flex-wrap items-center justify-between">
                                                                <span
                                                                    class="font-lora text-[14px] text-secondary leading-none">Giá: `+ transferToVNDPrice(paramRealEstate1) +`</span>

                                                                <span class="flex flex-wrap items-center">
                                                                    <button
                                                                        class="mr-[15px] text-[#B1AEAE] hover:text-secondary">
                                                                        <svg width="16" height="16" viewBox="0 0 16 16"
                                                                            fill="currentColor"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd"
                                                                                clip-rule="evenodd"
                                                                                d="M13.1667 11.6667C12.8572 11.6667 12.5605 11.7896 12.3417 12.0084C12.1229 12.2272 12 12.5239 12 12.8334C12 13.1428 12.1229 13.4395 12.3417 13.6583C12.5605 13.8771 12.8572 14 13.1667 14C13.4761 14 13.7728 13.8771 13.9916 13.6583C14.2104 13.4395 14.3333 13.1428 14.3333 12.8334C14.3333 12.5239 14.2104 12.2272 13.9916 12.0084C13.7728 11.7896 13.4761 11.6667 13.1667 11.6667ZM11 12.8334C11 12.2587 11.2283 11.7076 11.6346 11.3013C12.0409 10.895 12.592 10.6667 13.1667 10.6667C13.7413 10.6667 14.2924 10.895 14.6987 11.3013C15.1051 11.7076 15.3333 12.2587 15.3333 12.8334C15.3333 13.408 15.1051 13.9591 14.6987 14.3654C14.2924 14.7717 13.7413 15 13.1667 15C12.592 15 12.0409 14.7717 11.6346 14.3654C11.2283 13.9591 11 13.408 11 12.8334Z"
                                                                                fill="currentColor"></path>
                                                                            <path fill-rule="evenodd"
                                                                                clip-rule="evenodd"
                                                                                d="M9.26984 1.14667C9.36347 1.24042 9.41606 1.3675 9.41606 1.5C9.41606 1.6325 9.36347 1.75958 9.26984 1.85333L8.4565 2.66667H11.1665C11.8295 2.66667 12.4654 2.93006 12.9343 3.3989C13.4031 3.86774 13.6665 4.50363 13.6665 5.16667V11C13.6665 11.1326 13.6138 11.2598 13.5201 11.3536C13.4263 11.4473 13.2991 11.5 13.1665 11.5C13.0339 11.5 12.9067 11.4473 12.813 11.3536C12.7192 11.2598 12.6665 11.1326 12.6665 11V5.16667C12.6665 4.96968 12.6277 4.77463 12.5523 4.59264C12.4769 4.41065 12.3665 4.24529 12.2272 4.10601C12.0879 3.96672 11.9225 3.85623 11.7405 3.78085C11.5585 3.70547 11.3635 3.66667 11.1665 3.66667H8.45717L9.2705 4.48C9.36154 4.57434 9.41188 4.70067 9.41068 4.83177C9.40948 4.96287 9.35683 5.08825 9.26409 5.18091C9.17134 5.27357 9.04591 5.32609 8.91481 5.32717C8.78371 5.32825 8.65743 5.27779 8.56317 5.18667L6.8965 3.52C6.80287 3.42625 6.75028 3.29917 6.75028 3.16667C6.75028 3.03417 6.80287 2.90708 6.8965 2.81333L8.56317 1.14667C8.65692 1.05303 8.784 1.00044 8.9165 1.00044C9.049 1.00044 9.17609 1.05303 9.26984 1.14667ZM2.83317 4.33333C2.98638 4.33333 3.13809 4.30316 3.27963 4.24453C3.42118 4.1859 3.54979 4.09996 3.65813 3.99162C3.76646 3.88329 3.8524 3.75468 3.91103 3.61313C3.96966 3.47158 3.99984 3.31988 3.99984 3.16667C3.99984 3.01346 3.96966 2.86175 3.91103 2.7202C3.8524 2.57866 3.76646 2.45004 3.65813 2.34171C3.54979 2.23337 3.42118 2.14744 3.27963 2.08881C3.13809 2.03018 2.98638 2 2.83317 2C2.52375 2 2.22701 2.12292 2.00821 2.34171C1.78942 2.5605 1.6665 2.85725 1.6665 3.16667C1.6665 3.47609 1.78942 3.77283 2.00821 3.99162C2.22701 4.21042 2.52375 4.33333 2.83317 4.33333ZM4.99984 3.16667C4.99984 3.7413 4.77156 4.2924 4.36524 4.69873C3.95891 5.10506 3.40781 5.33333 2.83317 5.33333C2.25853 5.33333 1.70743 5.10506 1.30111 4.69873C0.894777 4.2924 0.666504 3.7413 0.666504 3.16667C0.666504 2.59203 0.894777 2.04093 1.30111 1.6346C1.70743 1.22827 2.25853 1 2.83317 1C3.40781 1 3.95891 1.22827 4.36524 1.6346C4.77156 2.04093 4.99984 2.59203 4.99984 3.16667Z"
                                                                                fill="currentColor"></path>
                                                                            <path fill-rule="evenodd"
                                                                                clip-rule="evenodd"
                                                                                d="M6.73016 14.8533C6.63653 14.7596 6.58394 14.6325 6.58394 14.5C6.58394 14.3675 6.63653 14.2404 6.73016 14.1467L7.5435 13.3333H4.8335C4.17046 13.3333 3.53457 13.0699 3.06573 12.6011C2.59689 12.1323 2.3335 11.4964 2.3335 10.8333V5C2.3335 4.86739 2.38617 4.74021 2.47994 4.64645C2.57371 4.55268 2.70089 4.5 2.8335 4.5C2.9661 4.5 3.09328 4.55268 3.18705 4.64645C3.28082 4.74021 3.3335 4.86739 3.3335 5V10.8333C3.3335 11.2312 3.49153 11.6127 3.77284 11.894C4.05414 12.1753 4.43567 12.3333 4.8335 12.3333H7.54283L6.7295 11.52C6.68176 11.4739 6.6437 11.4187 6.61752 11.3576C6.59135 11.2966 6.57759 11.231 6.57704 11.1646C6.5765 11.0982 6.58918 11.0324 6.61435 10.971C6.63952 10.9095 6.67667 10.8537 6.72364 10.8068C6.77061 10.7599 6.82645 10.7228 6.88791 10.6977C6.94937 10.6726 7.01521 10.6599 7.0816 10.6605C7.14799 10.6612 7.2136 10.675 7.27459 10.7012C7.33557 10.7274 7.39073 10.7656 7.43683 10.8133L9.1035 12.48C9.19713 12.5738 9.24972 12.7008 9.24972 12.8333C9.24972 12.9658 9.19713 13.0929 9.1035 13.1867L7.43683 14.8533C7.34308 14.947 7.216 14.9996 7.0835 14.9996C6.951 14.9996 6.82391 14.947 6.73016 14.8533Z"
                                                                                fill="currentColor"></path>
                                                                        </svg>
                                                                    </button>
                                                                    <button class="text-[#B1AEAE] hover:text-secondary">
                                                                        <svg width="16" height="16" viewBox="0 0 16 16"
                                                                            fill="currentColor"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                                                            <g clip-path="url(.clip0_656_640)">
                                                                                <path
                                                                                    d="M7.9999 2.74799L7.2829 2.01099C5.5999 0.280988 2.5139 0.877988 1.39989 3.05299C0.876895 4.07599 0.758895 5.55299 1.71389 7.43799C2.63389 9.25299 4.5479 11.427 7.9999 13.795C11.4519 11.427 13.3649 9.25299 14.2859 7.43799C15.2409 5.55199 15.1239 4.07599 14.5999 3.05299C13.4859 0.877988 10.3999 0.279988 8.7169 2.00999L7.9999 2.74799ZM7.9999 15C-7.33311 4.86799 3.27889 -3.04001 7.82389 1.14299C7.88389 1.19799 7.94289 1.25499 7.9999 1.31399C8.05632 1.25504 8.11503 1.19833 8.17589 1.14399C12.7199 -3.04201 23.3329 4.86699 7.9999 15Z"
                                                                                    fill="currentColor"></path>
                                                                            </g>
                                                                            <defs>
                                                                                <clipPath class="clip0_656_640">
                                                                                    <rect width="16" height="16"
                                                                                        fill="white"></rect>
                                                                                </clipPath>
                                                                            </defs>
                                                                        </svg>
                                                                    </button>
                                                                </span>
                                                            </li>
                                                        </ul>


                                                    </div>
                                                </div>

                                                <div
                                                    class="overflow-hidden rounded-md drop-shadow-[0px_2px_3px_rgba(0,0,0,0.1)] bg-[#FFFDFC] text-center">
                                                    <div class="relative">
                                                        <a href="properties-details.html?id=`+ paramRealEstate2.id +`" class="block">
                                                            <img src="asset/img/realestate/`+ vFirstPhoto2 +`"
                                                                class="w-full h-full" loading="lazy" width="370"
                                                                height="266" alt="@@title">
                                                        </a>

                                                    </div>

                                                    <div class="pt-[15px] pb-[20px] px-[20px] text-left">
                                                        <h3 style="height: 3.5em;overflow: hidden;text-overflow: ellipsis;"><a href="properties-details.html?id=`+ paramRealEstate2.id +`"
                                                                class="font-lora leading-tight text-[18px] text-primary">`+ paramRealEstate2.title +`</a></h3>
                                                        <h4 class="leading-none"><a href="properties-details.html?id=`+ paramRealEstate2.id +`"
                                                                class="font-light text-[14px] leading-[1.75] text-primary underline">` + transferLocation(paramRealEstate2) + `</a></h4>
                                                        <ul class="mt-[10px]">
                                                            <li class="flex flex-wrap items-center justify-between">
                                                                <span
                                                                    class="font-lora text-[14px] text-secondary leading-none">Giá: `+ transferToVNDPrice(paramRealEstate2) +`</span>

                                                                <span class="flex flex-wrap items-center">
                                                                    <button
                                                                        class="mr-[15px] text-[#B1AEAE] hover:text-secondary">
                                                                        <svg width="16" height="16" viewBox="0 0 16 16"
                                                                            fill="currentColor"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                                                            <path fill-rule="evenodd"
                                                                                clip-rule="evenodd"
                                                                                d="M13.1667 11.6667C12.8572 11.6667 12.5605 11.7896 12.3417 12.0084C12.1229 12.2272 12 12.5239 12 12.8334C12 13.1428 12.1229 13.4395 12.3417 13.6583C12.5605 13.8771 12.8572 14 13.1667 14C13.4761 14 13.7728 13.8771 13.9916 13.6583C14.2104 13.4395 14.3333 13.1428 14.3333 12.8334C14.3333 12.5239 14.2104 12.2272 13.9916 12.0084C13.7728 11.7896 13.4761 11.6667 13.1667 11.6667ZM11 12.8334C11 12.2587 11.2283 11.7076 11.6346 11.3013C12.0409 10.895 12.592 10.6667 13.1667 10.6667C13.7413 10.6667 14.2924 10.895 14.6987 11.3013C15.1051 11.7076 15.3333 12.2587 15.3333 12.8334C15.3333 13.408 15.1051 13.9591 14.6987 14.3654C14.2924 14.7717 13.7413 15 13.1667 15C12.592 15 12.0409 14.7717 11.6346 14.3654C11.2283 13.9591 11 13.408 11 12.8334Z"
                                                                                fill="currentColor"></path>
                                                                            <path fill-rule="evenodd"
                                                                                clip-rule="evenodd"
                                                                                d="M9.26984 1.14667C9.36347 1.24042 9.41606 1.3675 9.41606 1.5C9.41606 1.6325 9.36347 1.75958 9.26984 1.85333L8.4565 2.66667H11.1665C11.8295 2.66667 12.4654 2.93006 12.9343 3.3989C13.4031 3.86774 13.6665 4.50363 13.6665 5.16667V11C13.6665 11.1326 13.6138 11.2598 13.5201 11.3536C13.4263 11.4473 13.2991 11.5 13.1665 11.5C13.0339 11.5 12.9067 11.4473 12.813 11.3536C12.7192 11.2598 12.6665 11.1326 12.6665 11V5.16667C12.6665 4.96968 12.6277 4.77463 12.5523 4.59264C12.4769 4.41065 12.3665 4.24529 12.2272 4.10601C12.0879 3.96672 11.9225 3.85623 11.7405 3.78085C11.5585 3.70547 11.3635 3.66667 11.1665 3.66667H8.45717L9.2705 4.48C9.36154 4.57434 9.41188 4.70067 9.41068 4.83177C9.40948 4.96287 9.35683 5.08825 9.26409 5.18091C9.17134 5.27357 9.04591 5.32609 8.91481 5.32717C8.78371 5.32825 8.65743 5.27779 8.56317 5.18667L6.8965 3.52C6.80287 3.42625 6.75028 3.29917 6.75028 3.16667C6.75028 3.03417 6.80287 2.90708 6.8965 2.81333L8.56317 1.14667C8.65692 1.05303 8.784 1.00044 8.9165 1.00044C9.049 1.00044 9.17609 1.05303 9.26984 1.14667ZM2.83317 4.33333C2.98638 4.33333 3.13809 4.30316 3.27963 4.24453C3.42118 4.1859 3.54979 4.09996 3.65813 3.99162C3.76646 3.88329 3.8524 3.75468 3.91103 3.61313C3.96966 3.47158 3.99984 3.31988 3.99984 3.16667C3.99984 3.01346 3.96966 2.86175 3.91103 2.7202C3.8524 2.57866 3.76646 2.45004 3.65813 2.34171C3.54979 2.23337 3.42118 2.14744 3.27963 2.08881C3.13809 2.03018 2.98638 2 2.83317 2C2.52375 2 2.22701 2.12292 2.00821 2.34171C1.78942 2.5605 1.6665 2.85725 1.6665 3.16667C1.6665 3.47609 1.78942 3.77283 2.00821 3.99162C2.22701 4.21042 2.52375 4.33333 2.83317 4.33333ZM4.99984 3.16667C4.99984 3.7413 4.77156 4.2924 4.36524 4.69873C3.95891 5.10506 3.40781 5.33333 2.83317 5.33333C2.25853 5.33333 1.70743 5.10506 1.30111 4.69873C0.894777 4.2924 0.666504 3.7413 0.666504 3.16667C0.666504 2.59203 0.894777 2.04093 1.30111 1.6346C1.70743 1.22827 2.25853 1 2.83317 1C3.40781 1 3.95891 1.22827 4.36524 1.6346C4.77156 2.04093 4.99984 2.59203 4.99984 3.16667Z"
                                                                                fill="currentColor"></path>
                                                                            <path fill-rule="evenodd"
                                                                                clip-rule="evenodd"
                                                                                d="M6.73016 14.8533C6.63653 14.7596 6.58394 14.6325 6.58394 14.5C6.58394 14.3675 6.63653 14.2404 6.73016 14.1467L7.5435 13.3333H4.8335C4.17046 13.3333 3.53457 13.0699 3.06573 12.6011C2.59689 12.1323 2.3335 11.4964 2.3335 10.8333V5C2.3335 4.86739 2.38617 4.74021 2.47994 4.64645C2.57371 4.55268 2.70089 4.5 2.8335 4.5C2.9661 4.5 3.09328 4.55268 3.18705 4.64645C3.28082 4.74021 3.3335 4.86739 3.3335 5V10.8333C3.3335 11.2312 3.49153 11.6127 3.77284 11.894C4.05414 12.1753 4.43567 12.3333 4.8335 12.3333H7.54283L6.7295 11.52C6.68176 11.4739 6.6437 11.4187 6.61752 11.3576C6.59135 11.2966 6.57759 11.231 6.57704 11.1646C6.5765 11.0982 6.58918 11.0324 6.61435 10.971C6.63952 10.9095 6.67667 10.8537 6.72364 10.8068C6.77061 10.7599 6.82645 10.7228 6.88791 10.6977C6.94937 10.6726 7.01521 10.6599 7.0816 10.6605C7.14799 10.6612 7.2136 10.675 7.27459 10.7012C7.33557 10.7274 7.39073 10.7656 7.43683 10.8133L9.1035 12.48C9.19713 12.5738 9.24972 12.7008 9.24972 12.8333C9.24972 12.9658 9.19713 13.0929 9.1035 13.1867L7.43683 14.8533C7.34308 14.947 7.216 14.9996 7.0835 14.9996C6.951 14.9996 6.82391 14.947 6.73016 14.8533Z"
                                                                                fill="currentColor"></path>
                                                                        </svg>
                                                                    </button>
                                                                    <button class="text-[#B1AEAE] hover:text-secondary">
                                                                        <svg width="16" height="16" viewBox="0 0 16 16"
                                                                            fill="currentColor"
                                                                            xmlns="http://www.w3.org/2000/svg">
                                                                            <g clip-path="url(.clip0_656_640)">
                                                                                <path
                                                                                    d="M7.9999 2.74799L7.2829 2.01099C5.5999 0.280988 2.5139 0.877988 1.39989 3.05299C0.876895 4.07599 0.758895 5.55299 1.71389 7.43799C2.63389 9.25299 4.5479 11.427 7.9999 13.795C11.4519 11.427 13.3649 9.25299 14.2859 7.43799C15.2409 5.55199 15.1239 4.07599 14.5999 3.05299C13.4859 0.877988 10.3999 0.279988 8.7169 2.00999L7.9999 2.74799ZM7.9999 15C-7.33311 4.86799 3.27889 -3.04001 7.82389 1.14299C7.88389 1.19799 7.94289 1.25499 7.9999 1.31399C8.05632 1.25504 8.11503 1.19833 8.17589 1.14399C12.7199 -3.04201 23.3329 4.86699 7.9999 15Z"
                                                                                    fill="currentColor"></path>
                                                                            </g>
                                                                            <defs>
                                                                                <clipPath class="clip0_656_640">
                                                                                    <rect width="16" height="16"
                                                                                        fill="white"></rect>
                                                                                </clipPath>
                                                                            </defs>
                                                                        </svg>
                                                                    </button>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>`
        paramTag.append(vSlideBox);
    }


    // Hàm gọi API load dữ liệu vào select province
    function callApiToLoadDataToSelectProvince() {
        $.ajax({
            type: "GET",
            url: gBASE_URL_PROVINCE + "list",
            async: false,
            success: function (paramRes) {
                loadDataToSelectProvince(paramRes);
            },
            error: function (paramError) {
                console.log(paramError.status);
            }
        });
    }

    // Hàm load dữ liệu province trả về từ API để thêm vào thẻ select province
    // Input: Thẻ select cần thêm dữ liệu vào, dữ liệu province
    // Output: Load toàn bộ dữ liệu trả về vào thẻ
    function loadDataToSelectProvince(paramListProvinces) {
        let vSelectProvinceFilter = $("#select-province-filter");
        vSelectProvinceFilter.empty();
        vSelectProvinceFilter.selectric('destroy');
        $("<option>", {
            value: "",
            text: "Tỉnh, thành phố"
        }).appendTo(vSelectProvinceFilter);
        for (let bI = 0; bI < paramListProvinces.length; bI++) {
            $("<option>", {
                value: paramListProvinces[bI].id,
                text: paramListProvinces[bI].name
            }).appendTo(vSelectProvinceFilter);
        }
        vSelectProvinceFilter.selectric();
    }

    // Hàm xử lý sự kiện thay đổi nhu cầu khi thay đổi nhu cầu thì mức giá cũng sẽ thay đổi cho bộ lọc
    function onChangeRequest(paramSelect) {
        let vRequest = $(paramSelect).val();
        let vSelectPrice = $("#select-price");
        vSelectPrice.empty();
        vSelectPrice.selectric('destroy');
        $("<option>", {value: "", text: "Mức giá", "min": "0", "max": "999999999999"}).appendTo(vSelectPrice);   
        if(vRequest == "1") {
            $("<option>", {value: "0", text: "Dưới 1 triệu", "min": "0", "max": "1000000"}).appendTo(vSelectPrice);
            $("<option>", {value: "1", text: "Dưới 3 triệu", "min": "0", "max": "2999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "2", text: "Dưới 5 triệu", "min": "0", "max": "4999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "3", text: "Dưới 10 triệu", "min": "0", "max": "9999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "4", text: "Dưới 20 triệu", "min": "0", "max": "19999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "5", text: "Trên 20 triệu", "min": "20000000", "max": "999999999999"}).appendTo(vSelectPrice); 
        }
        else if(vRequest == "0") {
            $("<option>", {value: "0", text: "Dưới 500 triệu", "min": "0", "max": "499999999"}).appendTo(vSelectPrice);
            $("<option>", {value: "1", text: "Dưới 1 tỉ", "min": "0", "max": "999999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "2", text: "Dưới 2 tỉ", "min": "0", "max": "1999999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "3", text: "Dưới 5 tỉ", "min": "0", "max": "4999999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "4", text: "Dưới 10 tỉ", "min": "0", "max": "9999999999"}).appendTo(vSelectPrice);  
            $("<option>", {value: "5", text: "Trên 10 tỉ", "min": "10000000000", "max": "999999999999"}).appendTo(vSelectPrice);  
        }
        vSelectPrice.selectric();
    }
