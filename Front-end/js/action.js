// Lưu thông tin đăng nhập get cookie và xét phân quyền tài khoản
//Xử lý object trả về khi login thành công
function responseHandler(data) {
    //Lưu token vào cookie trong 1 ngày
    setCookie("token", data, 1);
    setTimeout(function () {
      // Navigate to index.html
      window.location.href = "index.html";
    }, 1000);
  }
  
  //Hàm setCookie đã giới thiệu ở bài trước
  function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  //Hàm get Cookie đã giới thiệu ở bài trước
  function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  
// Hàm gọi API để check xem token có hợp lệ hay không
// Input: Token 
// Output: Nếu hợp lệ gọi hàm hello user để xử lý, nếu không in lỗi ra console
function callApiToHelloUser() {
    let vHeader = {
      Authorization: "Token " + gTOKEN
    }
    $.ajax({
      type: "GET",
      url: gBASE_URL_USER + "hello1",
      headers: vHeader,
      success: function (paramRes) {
        helloUser(paramRes);
      },
      error: function (paramError) {
        window.location.href = "login.html";
        console.log(paramError.status);
      }
    });
  }
  
  // Hàm xử lý sau khi check token trả về
  // Input: Thông tin user lấy ra từ token trả về từ API
  // Ouput: Tùy theo role mà đưa đến từng vị trí thích hợp
  function helloUser(paramUser) {
    let vUserRole = paramUser.authorities[0];
    let vUsername = paramUser.username;
    $("#span-username-login").html(vUsername);
    if (vUserRole == "ROLE_CUSTOMER") {
      window.location.href = "index.html";
    }
    else if (vUserRole == "ROLE_HOME_SELLER") {
      $(".nav-navigate").attr("hidden", false);
      window.location.href = "addressmap.html";
    }
    else if (vUserRole == "ROLE_ADMIN") {
      $(".nav-navigate").attr("hidden", false);
      $(".nav-navigate-admin").attr("hidden", false);
      $(".btn-add").attr("hidden", false);
    }
  
    $(".nav-login").attr("hidden", true);
    $(".nav-view").attr("hidden", false);
  } 

  // Hàm kiểm tra email có đúng định dạng hay không
// Input: Email đăng ký
// Output: Sử dủng regex để check, trả về true nếu hợp lệ còn false nếu không
function validateEmail(paramEmail) {
    // Sử dụng regex để kiểm tra địa chỉ email
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (emailPattern.test(paramEmail)) {
      return true;
    }
    return false;
  }
  
  // Hàm kiểm tra số điện thoại có đúng định dạng hay không
  // Input: Số điện thoại đăng ký
  // Output: Sử dủng regex để check, trả về true nếu hợp lệ còn false nếu không
  function validatePhoneNumber(paramPhoneNumber) {
    var vPhoneNumberPattern = /^0[0-9]{9}$/;
    if (vPhoneNumberPattern.test(paramPhoneNumber)) {
      return true;
    }
    return false;
  }
  
  // Hàm validate mật khẩu phải có từ 6 kí tự trở lên bao gồm cả chữ lẫn số
  // Input: Mật khẩu người dùng
  // Output: Sử dủng regex để check, trả về true nếu hợp lệ còn false nếu không
  function validatePassword(paramPassword) {
    var passwordPattern = /^(?=.*[a-zA-Z])(?=.*[0-9]).{6,}$/;
    return passwordPattern.test(paramPassword);
  }

  // Hàm hiển thông báo  thành công
function alertSuccess(paramNotification) {
    gToast.fire({
      icon: 'success',
      title: paramNotification
    });
  }
  
  // Hàm thông báo lỗi validate theo toast
  function alertWarning(paramTitile) {
    gToast.fire({
      icon: 'warning',
      title: "" + paramTitile
    });
  }
  
  // Hàm hiển thông báo có lỗi trong quá trình đăng ký
  function alertError(paramTitile) {
    gToast.fire({
      icon: 'error',
      title: paramTitile
    });
  }
  